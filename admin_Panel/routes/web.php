<?php

use App\package;
use App\carrier;
use App\sender;
use App\package_recieving_detail;
use App\package_sending_detail;
/*use App\User;*/
use Illuminate\Support\Facades\Auth;
/*use Illuminate\Http\Request;*/

use Illuminate\Support\Facades\Input;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//experimrent not included---------------------------
Route::get('/exp', function () {
   /* $package = DB::table('packages')->get();*/

   $package = package::all();

    return view('exp/index',compact('package'));
});

//not needed------------------------

Route::get('/about', function () {
    return view('');
});

/*Route::get('welcome', function () {
    //whatever
})->middleware('auth');*/

//not needed

Route::get('/home', 'HomeController@index')->name('home');

//authenticate the routes

Auth::routes();






//Front page

Route::get('/', function () {
    return view('welcome');
});




//homepage or dashboard 

Route::get('/adminhome', function () {
    $i=0;
     
    $branchid= Auth::user()->branch_id;
    
     $homep_package = package::home_pickup($branchid);
     $officed_package = package::officedrop($branchid);
     $problem_package = package::problem_package($branchid);
     $carry_package = package::carry_package($branchid);
     $report_carriers = package::report_carriers($branchid);

     /*$packageid= Auth::package()->packageid;
     Session::put("id", $packageid );*/
     

    return view('adminhome',compact('i','homep_package','officed_package','branchid','problem_package','carry_package','report_carriers'));
})->middleware('auth');



//search users --- didnt edit till now


Route::get('/users', function () {
    return view('users');
})->middleware('auth');



//request carry packages

Route::get('/carryingpackages', function () {
    $i=0;
    $branchid= Auth::user()->branch_id;

    /*$carrying_packages = carrier::carrying_packages($branchid);*/

    $carrying_packages = package::carrying_packages($branchid);

    return view('carryingpackages',compact('i','carrying_packages'));
})->middleware('auth');



//change password --- didnt edit till now

Route::get('/change-password', function () {
    return view('change-password');
})->middleware('auth');




//edit profile --- didnt edit till now

Route::get('/edit-profile', function () {
    return view('edit-profile');
})->middleware('auth');




//forgot password --- didnt edit till now

Route::get('/forgot-password', function () {
    return view('forgot-password');
})->middleware('auth');



//show the package details --- database need to be addded

Route::get('/{task}', function ($id) {



    $task = package::package_id($id);
    $pid=0;
    $cid=0;
    $sid=0;
    foreach ($task as $task) {
        $pid= $task->packageid;
        $sid= $task->sender_id;
        $cid= $task->carrier_id;

    }

    $packa_rec_details = package_recieving_detail::recieving_detail($pid);
    $packa_sen_details = package_sending_detail::sending_detail($pid);
    $carrier_details = carrier::carrying_details($cid);
    $sender_details = sender::sender_details($cid);
    

    return view('invoice-view',compact('task','packa_rec_details','packa_sen_details','carrier_details','sender_details'));
})->middleware('auth');




//didnt edit till now---didnt edit

Route::get('/invoices', function () {
    return view('invoices');
})->middleware('auth');




//my login- --- didnt include-----------------------------

Route::get('/login2', function () {
    return view('login');
})->middleware('auth');



//branch recieving packages

Route::get('/ownbranchrecievingpackages', function () {
    $i=0;
    $branchid= Auth::user()->branch_id;

    /*$sending_packages = sender::sending_packages($branchid);*/
    
    $recieving_packages = package::branchpackagesrecieving($branchid);

    return view('ownbranchrecievingpackages',compact('recieving_packages'));
})->middleware('auth');




//Branch sending the packages

Route::get('/ownbranchsendingpackages', function () {
    $i;
    $branchid= Auth::user()->branch_id;

    /*$sending_packages = sender::sending_packages($branchid);*/
    
    $sending_packages = package::branchpackagessending($branchid);

    return view('ownbranchsendingpackages',compact('sending_packages'));
})->middleware('auth');




//profile ---didnt edit

Route::get('/profile', function () {
    return view('profile');
})->middleware('auth');




//register mine- ---not inlcuded

Route::get('/register2', function () {
    return view('register');
})->middleware('auth');




//view of the reports details ---didnt edit

Route::get('/reports-view', function () {
    return view('reports-view');
})->middleware('auth');




//view of the reports ---didnt edit

Route::get('/reports', function () {

    $branchid= Auth::user()->branch_id;

    /*$sending_packages = sender::sending_packages($branchid);*/
    
    $reported_packages = package::report($branchid);


    return view('reports',compact('reported_packages'));
})->middleware('auth');




//not included---------------------------

Route::get('/searchcarryresults', function () {
    return view('searchcarryresults');
})->middleware('auth');



//search all the sending packages fromcurrent branch

Route::get('/searchsend', function () {
    $branchid= Auth::user()->branch_id;

    /*$sending_packages = sender::sending_packages($branchid);*/
    
    $sending_packages = package::branchpackagessending($branchid);

    return view('searchsend',compact('sending_packages'));

})->middleware('auth');




//search all the carrying packages fromcurrent branch

Route::get('/searchcarry', function () {

    $branchid= Auth::user()->branch_id;

    /*$sending_packages = sender::sending_packages($branchid);*/
    
    $carrying_packages = package::branchcarrying_packages($branchid);

    return view('searchcarry',compact('carrying_packages'));
})->middleware('auth');




//not included-------------------------

Route::get('/searchsendresults', function () {
    return view('searchsendresults');
})->middleware('auth');





//requested sending packages in certain branch

Route::get('/sendingpackages', function () {

    $i=0;
    $branchid= Auth::user()->branch_id;

    /*$sending_packages = sender::sending_packages($branchid);*/
    
    $sending_packages = package::sending_packages($branchid);

    return view('sendingpackages',compact('i','sending_packages'));
    
})->middleware('auth');





//home pickup of packages by the branch

Route::get('/homepickup', function () {
    $branchid= Auth::user()->branch_id;
    
    $pickup_package = package::home_pickup($branchid);    

    return view('homepickup',compact('pickup_package'));
})->middleware('auth');




//problem with the packages --- didnt edit

Route::get('/sendingpackagesproblem', function () {
    $branchid= Auth::user()->branch_id;

    $problem_package = package::problem_package($branchid);

    return view('sendingpackagesproblem',compact('problem_package'));
})->middleware('auth');




//delivery package by the branch

Route::get('/deliverypackagetooffice', function () {
    $branchid= Auth::user()->branch_id;

    $officed_package = package::officedrop($branchid);

    return view('deliverypackagetooffice',compact('officed_package'));
})->middleware('auth');




//settings --didnt edit

Route::get('/settings', function () {
    return view('settings');
})->middleware('auth');




//get method to track all the packages
Route::get('/tracking', function () {
    $trackid='null';
    $tracking_packages = package::tracking_packages($trackid);
    /*$trackid=0;
    if($trackid==0){
        echo 'nothing found';
        $tracking_packages = package::tracking_packages('null');
    }
        
    else{
        echo 'a';
        $tracking_packages = package::tracking_packages($trackid);

    }*/

    
    
    /*$trackid=$_POST["id"];
    $trackid= Input::get('id');

    

    if(count($tracking_packages) > 0)
        return view('tracking',compact('trackid','tracking_packages'))->withDetails($tracking_packages)->withQuery ( $trackid );
    else 
        return view ('tracking',compact('trackid','tracking_packages'))->withMessage('No Details found. Try to search again !');*/
        /*,compact('trackid','tracking_packages')*/



    return view('tracking',compact('tracking_packages') );
})->middleware('auth');





//post method to track all the packages

Route::post('/tracking', function () {
    
    $trackid= Input::get('id');
    $tracking_packages = package::tracking_packages($trackid);

    //echo 'bb';
    /*var_dump('search results');
    foreach ($tracking_packages as $packag ) {
        
        var_dump($packag->sender_name);
    }*/

    return view('tracking',compact('tracking_packages'));
})->middleware('auth');



/*Route::get('/trackingresults', function () {
    $tracking_packages = package::tracking_packages();
    return view('trackingresults',compact('tracking_packages'));
})->middleware('auth');*/