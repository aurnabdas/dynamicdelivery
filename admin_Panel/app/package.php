<?php

namespace App;

use Illuminate\Database\Eloquent\Model;



class package extends Model
{

    //---------homepickup
	public  function scopehome_pickup($query,$branchid){
        
    	return $query->where('branch_id_from','=',$branchid)->where('home_pickup','=','yes')->get();
    }

    //---------homepickup
    public  function scopepickuppackages($query,$branchid){
        
        return $query->where('branch_id_from','=',$branchid)->where('home_pickup','=','yes')->get();
    }

    /*public static function homepickup(){
    	return static::where('home_pickup','=','yes')->get();
    }*/

    //----------officedrop

     
    public  function scopeofficedrop($query,$branchid){
    	return $query->where('branch_id_from','=',$branchid)->where('office_drop','yes')->get();
    }
    /*public static function officedrop(){
        return static::where('office_drop','yes')->get();
    }*/

    /*public function scopeofficedrop($query){
        return $query->where('office_drop','=','yes')->get();
    }*/


    //--------------carrying request

    public  function scopebranchcarrying_packages($query,$branchid){

        return  $query->where('branch_id_from','=',$branchid)->where('carrier_id','!=',NULL)->get();
        /**/
        
    }


    

    //----------branch sending packages


     public  function scopebranchpackagessending($query, $branchid){
    	return $query->where('branch_id_from','=',$branchid)->get();
    }

    /*public static function branchpackagessending( $branchid){
        return static::where('branch_id_from','=','$branchid')->get();
    }*/
    


    //------------branch recieving packages
     public  function scopebranchpackagesrecieving($query, $branchid){
    	return $query->where('branch_id_to','=',$branchid)->get();
    }

    //--------------total packages
    public  function scopetotalpackages($query){
    	return Model::get()->count();
    }

    //-----------total package certain branch
    public  function scopetotalpackage_certainbranch($query,$branchid){

    	return Model::branchpackagessending($branchid)->count();

    	/*return Model::get()->where('branch_id_to','=',$branchid)->count();*/
    }

    //--------------problem packages

    public  function scopeproblem_package($query,$branchid){

        return  $query->where('branch_id_from','=',$branchid)->where('report','=',1)->get();

        
    }


    //--------------CARRYING PACKAGES

    public  function scopecarry_package($query,$branchid){

        return  $query->where('branch_id_from','=',$branchid)->where('carrier_id','!=',NULL)->get();


        /*carrier_pickup_dest*/
    }


    //--------------reported carriers

    public  function scopereport_carriers($query,$branchid){

        return  $query->where('branch_id_from','=',$branchid)->where('report','=',1)->where('carrier_id','!=',NULL)->get();

        
    }

    //--------------reports

    public  function scopereport($query,$branchid){

        return  $query->where('branch_id_from','=',$branchid)->where('report','=',1)->where('carrier_id','!=',NULL)->get();

        
    }

    //--------------sending request

    public  function scopesending_packages($query,$branchid){

        return  $query->where('branch_id_from','=',$branchid)->where('carrier_id','=',NULL)->get();

        
    }

    //--------------carrying request

    public  function scopecarrying_packages($query,$branchid){

        return  $query->where('branch_id_from','=',$branchid)->where('carrier_id','!=',NULL)->get();

        
    }

     //--------------tracking request

    public  function scopetracking_packages($query,$trackid){

        return  $query->where('tracking_num','=',$trackid)->get();

        
    }

    //--------------tracking request

    public  function scopepackage_id($query,$id){

        return  $query->where('packageid','=',$id)->get();

        
    }






}

