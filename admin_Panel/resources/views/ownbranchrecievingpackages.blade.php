 @extends('layouts.insideapp')

<div class="page-wrapper">
                <div class="content container-fluid">
					<div class="row">
						<div class="col-sm-8">
							<h4 class="page-title">Packages being carried</h4>
						</div>
					</div>
					<div class="row filter-row">
						<form action="tracking" method="post" role="search">
							{{ csrf_field() }}
								
							<div class="col-sm-3 col-md-2 col-xs-6">  
								<div class="form-group form-focus">
									<label class="control-label">Package Id</label>
									<input type="text" class="form-control floating" />
								</div>
						   </div>
						
						

							<!-- <div class="col-sm-3 col-xs-6">  
								<div class="form-group form-focus">
									<label class="control-label">From</label>
									<div class="cal-icon"><input class="form-control floating datetimepicker" type="text"></div>
								</div>
							</div>
							<div class="col-sm-3 col-xs-6">  
								<div class="form-group form-focus">
									<label class="control-label">To</label>
									<div class="cal-icon"><input class="form-control floating datetimepicker" type="text"></div>
								</div>
							</div> -->
							<!-- <div class="col-sm-3 col-xs-6"> 
								<div class="form-group form-focus select-focus">
									<label class="control-label">Status</label>
									<select class="select floating"> 
										<option value="">Select Status</option>
										<option value="">Pending</option>
										<option value="1">Paid</option>
										<option value="1">Partially Paid</option>
									</select>
								</div>
							</div> -->
							<div class="col-sm-3 col-xs-6">  
								<button type="submit" class="btn btn-success btn-block" >
									<!-- <a href="trackingresults" > --> Search  <!-- </a>  --> 
								</button>
							</div> 
						</form>

                    </div>
					<div class="row">
						<div class="col-md-12">
							<div class="table-responsive">
								<table class="table table-striped custom-table m-b-0 datatable">
									<thead>
										<tr>
											<th>#</th>
											<<th>Package ID</th>
											<th>Sender</th>
											<th>Pickup Date</th>
											<th>Total</th>
											<th>Pickup Status</th>
											<th>Carrier</th>
											<th>Created</th>
											<th class="text-right">Actions</th>
										</tr>
									</thead>
									<tbody>
										@foreach( $recieving_packages as $packag)
                                                  
                                                    
                                            <tr>
                                                <td><a href="invoice-view">{{ $packag->packageid }}</a></td>
                                                <td>
                                                    <h2><a href="#">{{ $packag->sender_name }}</a></h2>
                                                </td>
                                                <td>{{ $packag->sending_date }}</td>
                                                <td>{{ $packag->arrival_date }}</td>
                                                <td>{{ $packag->total_price }}</td>
                                                <td>
                                                	 @if($packag->completed == 1  )
                                                            <span class="label label-success-border">Success</span>
                                         			@else
                                                            <span class="label label-warning-border">Pending</span>
                                                    @endif
                                                    
                                                </td>
                                                <td>
                                                    @if($packag->allocated == 1)
                                                   		 <span class="label label-success-border">Allocated</span>
                                   					@else
                                                        <span class="label label-warning-border">Not Allocated</span>
                                                    @endif
                                                                     
                                                                    
                                                </td>
                                                <td>
                                                	@if($packag->active == 0)
                                                                Inactive
                                             		@else
                                                                Active
                                                            @endif
                                                </td>
                                            </tr>
                                                                                             
                                        @endforeach

                                       

										
									</tbody>
								</table>
							</div>
						</div>
					</div>
                </div>
            </div>
        