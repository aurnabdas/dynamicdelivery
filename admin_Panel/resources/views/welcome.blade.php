<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Dynamic Delivery</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body style="background-image: url('assets/img/bgg.jpg'); width: 100%; height: 100%;background-position: center;background-repeat: no-repeat;background-size: cover;"> <!-- filter: blur(.5px); -->
        <div class="welcome-body">
            <div class="flex-center position-ref full-height" >
                @if (Route::has('login'))
                    <div class="top-right links">
                        @auth
                            <a href="{{ url('/adminhome') }}" style="color: red;">Home</a>
                        @else
                            <a href="{{ route('login') }}" style="color: red;">Login</a>
                           <!--  <a href="{{ route('register') }}">Register</a> -->
                        @endauth
                    </div>
                @endif
                <!-- <img src="assets/img/video-call.jpg" width="100%" length="100%" style="float: none;"> -->
                <div class="content" >  <!-- style="background-image: url(assets/img/video-call.jpg);" -->
                    <div class="title m-b-md" style="color: #ff4d39;text-shadow: 10px 2px #131722;font-weight: 650;">
                        Dynamic Delivery Admin
                    </div>

                    
                </div>
            </div>
            
        </div>
        
    </body>
</html>
