 @extends('layouts.insideapp')

<div class="page-wrapper">
                <div class="content container-fluid">
					<div class="row">
						<div class="col-sm-8">
							<h4 class="page-title">Search for sending packages</h4>
						</div>
					</div>
					<div class="row filter-row">
						<form action="" method="post" role="search">
							{{ csrf_field() }}
								
							<div class="col-sm-3 col-md-2 col-xs-6">  
								<div class="form-group form-focus">
									<label class="control-label">Package Id</label>
									<input type="text" class="form-control floating" />
								</div>
						   </div>
						
						

							<!-- <div class="col-sm-3 col-xs-6">  
								<div class="form-group form-focus">
									<label class="control-label">From</label>
									<div class="cal-icon"><input class="form-control floating datetimepicker" type="text"></div>
								</div>
							</div>
							<div class="col-sm-3 col-xs-6">  
								<div class="form-group form-focus">
									<label class="control-label">To</label>
									<div class="cal-icon"><input class="form-control floating datetimepicker" type="text"></div>
								</div>
							</div> -->
							<!-- <div class="col-sm-3 col-xs-6"> 
								<div class="form-group form-focus select-focus">
									<label class="control-label">Status</label>
									<select class="select floating"> 
										<option value="">Select Status</option>
										<option value="">Pending</option>
										<option value="1">Paid</option>
										<option value="1">Partially Paid</option>
									</select>
								</div>
							</div> -->
							<div class="col-sm-3 col-xs-6">  
								<button type="submit" class="btn btn-success btn-block" >
									<!-- <a href="trackingresults" > --> Search  <!-- </a>  --> 
								</button>
							</div> 
						</form>



                    <div class="row">
						<div class="col-md-12">
							<div class="table-responsive">
								<table class="table table-striped custom-table m-b-0 datatable">
									<thead>
										<tr>
											
											<<th>Package ID</th>
											<th>Sender</th>
											<th>Sending Date</th>
											<th>Arrival Date</th>
											<th>Total</th>											
											<th>Carrier</th>
											<th>Status</th>
											<th>Created</th>
											
										</tr>
									</thead>
									<tbody>


										@foreach( $sending_packages as $packag)
                                                    
                                                            <tr>
		                                                        <td><a href="invoice-view">{{ $packag->packageid }}</a></td>
		                                                        
		                                                        <td>
		                                                            <h2><a href="#">{{ $packag->sender_name }} </a></h2>
		                                                        </td>
		                                                        <td>
		                                                        	@if($packag->sending_date == '')
                                                                            <span class="label label-warning-border">Not Allocated</span>

                                                                        @else
                                                                            {{ $packag->sending_date }}
                                                                        @endif
		                                                        </td>
		                                                        <td>
		                                                        	@if($packag->arrival_date == '')
                                                                            <span class="label label-warning-border">Not Allocated</span>

                                                                        @else
                                                                            {{ $packag->arrival_date }}
                                                                        @endif
		                                                        </td>
		                                                        <td>{{ $packag->total_price }}</td>
		                                                        
		                                                        <td>@if($packag->carrier_name == null)
		                                                        		<span class="label label-warning-border">Not Allocated</span>
                                                                    @else
                                                                        {{ $packag->carrier_name }}
                                                                    @endif
		                                                        </td>
		                                                        <td>
                                                                   
                                                                        @if($packag->completed == 0)
                                                                            <span class="label label-warning-border"> Pending </span>

                                                                        @else
                                                                           <span class="label label-success-border"> Success </span>
                                                                        @endif
                                                                    
                                                                    
                                                                </td>
                                                                <td>{{ $packag->created_at }}</td>
		                                                    </tr>                                                    
                                                @endforeach


										
										
									</tbody>
								</table>
							</div>
						</div>
					</div>
					
                </div>
				
            </div>
        