@extends('layouts.app')

@section('content')
<div class="page-wrapper">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-lg-3">
                            <div class="dash-widget clearfix card-box">
                                <span class="dash-widget-icon"><i class="fa fa-user" aria-hidden="true"></i></span>
                                <div class="dash-widget-info">
                                    <h3>112</h3>
                                    <span>Total Users</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-lg-3">
                            <div class="dash-widget clearfix card-box">
                                <span class="dash-widget-icon"><i class="fa fa-user" aria-hidden="true"></i></span>
                                <div class="dash-widget-info">
                                    <h3>44k</h3>
                                    <span>Local Clients</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-lg-3">
                            <div class="dash-widget clearfix card-box">
                                <span class="dash-widget-icon"><i class="fa fa-cubes"></i></span>
                                <div class="dash-widget-info">
                                    <h3>37</h3>
                                    <span>Incoming packages</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-lg-3">
                            <div class="dash-widget clearfix card-box">
                                <span class="dash-widget-icon"><i class="fa fa-cubes" aria-hidden="true"></i></span>
                                <div class="dash-widget-info">
                                    <h3>218</h3>
                                    <span>Outgoing Packages</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-sm-6 text-center">
                                    <div class="card-box">
                                        <div id="area-chart" ></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 text-center">
                                    <div class="card-box">
                                        <div id="pie-chart" ></div>
                                    </div>
                                </div>
                                <div class=" col-sm-12 text-center">
                                    <div class="card-box">
                                        <div id="line-chart"></div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div style="text-align: center;background-color: #ffc857;"> <!-- //#a1bcc6 -->
                        <hr>
                        <h4>Sending Packages</h4>
                        <hr>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="panel panel-table">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Home Pickup By Office</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped custom-table m-b-0">
                                            <thead>
                                                <tr>
                                                    <th>Package ID</th>
                                                    <th>Sender</th>
                                                    <th>Pickup Date</th>
                                                    <th>Total</th>
                                                    <th>Pickup Status</th>
                                                    <th>Carrier</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><a href="invoice-view.html">#INV-0001</a></td>
                                                    <td>
                                                        <h2><a href="#">Hazel Nutt</a></h2>
                                                    </td>
                                                    <td>28 Dec 2017</td>
                                                    <td>$380</td>
                                                    <td>
                                                        <span class="label label-warning-border">Pending</span>
                                                        <!-- <span class="label label-danger-border">Unpaid</span> -->
                                                    </td>
                                                    <td>
                                                        <span class="label label-warning-border">Not Allocated</span>
                                                        <!-- <span class="label label-danger-border">Unpaid</span> -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><a href="invoice-view.html">#INV-0002</a></td>
                                                    <td>
                                                        <h2><a href="#">curli Turner</a></h2>
                                                    </td>
                                                    <td>17 Dec 2017</td>
                                                    <td>$500</td>
                                                    <td>
                                                        <span class="label label-warning-border">Pending</span>
                                                    </td>
                                                    <td>
                                                        <span class="label label-warning-border">Not Allocated</span>
                                                        <!-- <span class="label label-danger-border">Unpaid</span> -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><a href="invoice-view.html">#INV-0003</a></td>
                                                    <td>
                                                        <h2><a href="#">Ben trav</a></h2>
                                                    </td>
                                                    <td>30 Nov 2017</td>
                                                    <td>$60</td>
                                                    <td>
                                                        
                                                        <span class="label label-success-border">Done</span>
                                                    </td>
                                                    <td>
                                                        <span class="label label-warning-border">Not Allocated</span>
                                                        <!-- <span class="label label-danger-border">Unpaid</span> -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><a href="invoice-view.html">#INV-0004</a></td>
                                                    <td>
                                                        <h2><a href="#">Ben Dover</a></h2>
                                                    </td>
                                                    <td>4 Nov 2017</td>
                                                    <td>$60</td>
                                                    <td>
                                                        
                                                        <span class="label label-success-border">Done</span>
                                                    </td>
                                                    <td>
                                                        <span class="label label-success-border">Allocated</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <a href="homepickup.html" class="text-primary">View all Pickup by office</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-table">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Delivery Packages To Office</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">  
                                        <table class="table table-striped custom-table m-b-0">
                                            <thead>
                                                <tr>
                                                    <th>Package ID</th>
                                                    <th>Sender</th>
                                                    <th>Delivery Date</th>
                                                    <th>Total</th>
                                                    <th>Delivery Status</th>
                                                    <th>Carrier</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><a href="invoice-view.html">#INV-0101</a></td>
                                                    <td>
                                                        <h2><a href="#">Hazel Nutt</a></h2>
                                                    </td>
                                                    <td>28 Dec 2017</td>
                                                    <td>$380</td>
                                                    <td>
                                                        <span class="label label-warning-border">Pending</span>
                                                        <!-- <span class="label label-danger-border">Unpaid</span> -->
                                                    </td>
                                                    <td>
                                                        <span class="label label-warning-border">Not Allocated</span>
                                                        <!-- <span class="label label-danger-border">Unpaid</span> -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><a href="invoice-view.html">#INV-0102</a></td>
                                                    <td>
                                                        <h2><a href="#">Murli warner</a></h2>
                                                    </td>
                                                    <td>17 Dec 2017</td>
                                                    <td>$500</td>
                                                    <td>
                                                        <span class="label label-warning-border">Pending</span>
                                                    </td>
                                                    <td>
                                                        <span class="label label-warning-border">Not Allocated</span>
                                                        <!-- <span class="label label-danger-border">Unpaid</span> -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><a href="invoice-view.html">#INV-0103</a></td>
                                                    <td>
                                                        <h2><a href="#">K en trav</a></h2>
                                                    </td>
                                                    <td>30 Nov 2017</td>
                                                    <td>$60</td>
                                                    <td>
                                                        
                                                        <span class="label label-success-border">Done</span>
                                                    </td>
                                                    <td>
                                                        <span class="label label-warning-border">Not Allocated</span>
                                                        <!-- <span class="label label-danger-border">Unpaid</span> -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><a href="invoice-view.html">#INV-0104</a></td>
                                                    <td>
                                                        <h2><a href="#">Wen Dover</a></h2>
                                                    </td>
                                                    <td>4 Nov 2017</td>
                                                    <td>$60</td>
                                                    <td>
                                                        
                                                        <span class="label label-success-border">Done</span>
                                                    </td>
                                                    <td>
                                                        <span class="label label-success-border">Allocated</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <a href="payments.html" class="text-primary">View all Delivery packages to office</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="panel panel-table">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Problem Packages</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped custom-table m-b-0">
                                            <thead>
                                                <tr>
                                                    <th>Package ID</th>
                                                    <th>Sender</th>
                                                    <th>Carrier</th>
                                                    <th>Sending Date</th>
                                                    <th>Recieving Date</th>
                                                    <th>Problem type</th>
                                                    <th>Problem Status</th>
                                                    <th class="text-right">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><a href="invoice-view.html">#INV-0101</a></td>
                                                    <td>
                                                        <h2><a href="#">Hazel Nutt</a></h2>
                                                    </td>
                                                    <td>
                                                        <h2><a href="#">azel kutt</a></h2>
                                                    </td>
                                                    <td>28 Dec 2017</td>
                                                    <td>2 Jan 2018</td>
                                                    <td>
                                                        <span class="label label-danger-border">Severe</span>
                                                        <!-- <span class="label label-danger-border">Unpaid</span> -->
                                                    </td>
                                                    <td>
                                                        <div class="dropdown action-label">
                                                            <a class="btn btn-white btn-sm rounded dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-dot-circle-o text-danger"></i> Inactive <i class="caret"></i>
                                                            </a>
                                                            <ul class="dropdown-menu pull-right">
                                                                <li><a href="#"><i class="fa fa-dot-circle-o text-success"></i> Active</a></li>
                                                                <li><a href="#"><i class="fa fa-dot-circle-o text-danger"></i> Inactive</a></li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                    <td class="text-right">
                                                        <div class="dropdown">
                                                            <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                                                            <ul class="dropdown-menu pull-right">
                                                                <li><a href="#" title="Edit"><i class="fa fa-pencil m-r-5"></i> Edit</a></li>
                                                                <li><a href="#" title="Delete"><i class="fa fa-trash-o m-r-5"></i> Delete</a></li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><a href="invoice-view.html">#INV-0102</a></td>
                                                    <td>
                                                        <h2><a href="#">Murli warner</a></h2>
                                                    </td>
                                                    <td>
                                                        <h2><a href="#">Hazel Nutt</a></h2>
                                                    </td>
                                                    <td>17 Dec 2017</td>
                                                    <td>2 Jan 2018</td>
                                                    <td>
                                                        <span class="label label-warning-border">mediocore</span>
                                                    </td>
                                                    <td>
                                                        <div class="dropdown action-label">
                                                            <a class="btn btn-white btn-sm rounded dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-dot-circle-o text-success"></i> Active <i class="caret"></i>
                                                            </a>
                                                            <ul class="dropdown-menu pull-right">
                                                                <li><a href="#"><i class="fa fa-dot-circle-o text-success"></i> Active</a></li>
                                                                <li><a href="#"><i class="fa fa-dot-circle-o text-danger"></i> Inactive</a></li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                    <td class="text-right">
                                                        <div class="dropdown">
                                                            <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                                                            <ul class="dropdown-menu pull-right">
                                                                <li><a href="#" title="Edit"><i class="fa fa-pencil m-r-5"></i> Edit</a></li>
                                                                <li><a href="#" title="Delete"><i class="fa fa-trash-o m-r-5"></i> Delete</a></li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><a href="invoice-view.html">#INV-0103</a></td>
                                                    <td>
                                                        <h2><a href="#">K en trav</a></h2>
                                                    </td>
                                                    <td>
                                                        <h2><a href="#">Hazel Nutt</a></h2>
                                                    </td>
                                                    <td>17 Dec 2017</td>
                                                    <td>3 Jan 2018</td>
                                                    <td>
                                                        
                                                        <span class="label label-success-border">Solved</span>
                                                    </td>
                                                    <td>
                                                        <div class="dropdown action-label">
                                                            <a class="btn btn-white btn-sm rounded dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-dot-circle-o text-success"></i> Active <i class="caret"></i>
                                                            </a>
                                                            <ul class="dropdown-menu pull-right">
                                                                <li><a href="#"><i class="fa fa-dot-circle-o text-success"></i> Active</a></li>
                                                                <li><a href="#"><i class="fa fa-dot-circle-o text-danger"></i> Inactive</a></li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                    <td class="text-right">
                                                        <div class="dropdown">
                                                            <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                                                            <ul class="dropdown-menu pull-right">
                                                                <li><a href="#" title="Edit"><i class="fa fa-pencil m-r-5"></i> Edit</a></li>
                                                                <li><a href="#" title="Delete"><i class="fa fa-trash-o m-r-5"></i> Delete</a></li>
                                                            </ul>
                                                        </div>
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td><a href="invoice-view.html">#INV-0104</a></td>
                                                    <td>
                                                        <h2><a href="#">Wen Dover</a></h2>
                                                    </td>
                                                    <td>
                                                        <h2><a href="#">Hazel Nutt</a></h2>
                                                    </td>
                                                    <td>17 Dec 2017</td>
                                                    <td>4 Nov 2017</td>
                                                    <td>
                                                        
                                                        <span class="label label-warning-border">Mediocore</span>
                                                    </td>
                                                    <td>
                                                        <div class="dropdown action-label">
                                                            <a class="btn btn-white btn-sm rounded dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-dot-circle-o text-danger"></i> Inactive <i class="caret"></i>
                                                            </a>
                                                            <ul class="dropdown-menu pull-right">
                                                                <li><a href="#"><i class="fa fa-dot-circle-o text-success"></i> Active</a></li>
                                                                <li><a href="#"><i class="fa fa-dot-circle-o text-danger"></i> Inactive</a></li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                    <td class="text-right">
                                                        <div class="dropdown">
                                                            <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                                                            <ul class="dropdown-menu pull-right">
                                                                <li><a href="#" title="Edit"><i class="fa fa-pencil m-r-5"></i> Edit</a></li>
                                                                <li><a href="#" title="Delete"><i class="fa fa-trash-o m-r-5"></i> Delete</a></li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <a href="sendingpackagesproblem.html" class="text-primary">View all Problem Packages</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="panel panel-table">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Package progress</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped custom-table m-b-0">
                                            <thead>
                                                <tr>
                                                    <th class="col-md-3">Package Id </th>
                                                    <th class="col-md-3">Progress</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><a href="invoice-view.html">#INV-0004</a></td>
                                                    <td>
                                                        <div class="progress progress-xs progress-striped">
                                                            <div class="progress-bar bg-success" role="progressbar" data-toggle="tooltip" title="65%" style="width: 65%"></div>
                                                        </div>
                                                    </td>                                                   
                                                </tr>
                                                <tr>
                                                    <td><a href="invoice-view.html">#INV-0005</a></td>
                                                    <td>
                                                        <div class="progress progress-xs progress-striped">
                                                            <div class="progress-bar bg-success" role="progressbar" data-toggle="tooltip" title="15%" style="width: 15%"></div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><a href="invoice-view.html">#INV-0104</a></td>
                                                    <td>
                                                        <div class="progress progress-xs progress-striped">
                                                            <div class="progress-bar bg-success" role="progressbar" data-toggle="tooltip" title="49%" style="width: 49%"></div>
                                                        </div>
                                                    </td>                                                   
                                                </tr>
                                                <tr>
                                                    <td><a href="invoice-view.html">#INV-0105</a></td>
                                                    <td>
                                                        <div class="progress progress-xs progress-striped">
                                                            <div class="progress-bar bg-success" role="progressbar" data-toggle="tooltip" title="88%" style="width: 88%"></div>
                                                        </div>
                                                    </td>
                                                    
                                                </tr>
                                                <tr>
                                                    <td><a href="invoice-view.html">#INV-0106</a></td>
                                                    <td>
                                                        <div class="progress progress-xs progress-striped">
                                                            <div class="progress-bar bg-success" role="progressbar" data-toggle="tooltip" title="100%" style="width: 100%"></div>
                                                        </div>
                                                    </td>                                                   
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <a href="sendingpackages.html" class="text-primary">View all Packages Progress</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="text-align: center;background-color: #ffc857 ;"> <!-- //#a1bcc6 -->
                        <hr>
                        <h4>Carrying Packages</h4>
                        <hr>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-table">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Packages being carried</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped custom-table m-b-0">
                                            <thead>
                                                <tr>
                                                    <th>Package ID</th>
                                                    <th>Carrier</th>
                                                    <th>Pickup Date</th>
                                                    <th>Delevery Date</th>
                                                    <th>Total</th>
                                                    <th>Status</th>
                                                    <th>Payment Status </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><a href="invoice-view.html">#INV-0001</a></td>
                                                    <td>
                                                        <h2><a href="#">Hazel Nutt</a></h2>
                                                    </td>
                                                    <td>2 Dec 2017</td>
                                                    <td>28 Dec 2017</td>                                                    
                                                    <td>$380</td>
                                                    <td>
                                                        <span class="label label-success-border">Done</span>
                                                        <!-- <span class="label label-danger-border">Unpaid</span> -->
                                                    </td>
                                                    <td>
                                                        <span class="label label-success-border">Paid</span>
                                                        <!-- <span class="label label-danger-border">Unpaid</span> -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><a href="invoice-view.html">#INV-0002</a></td>
                                                    <td>
                                                        <h2><a href="#">curli Turner</a></h2>
                                                    </td>
                                                    <td>17 Dec 2017</td>
                                                    <<td>28 Dec 2017</td>                                                   
                                                    <td>$380</td>
                                                    <td>
                                                        <span class="label label-warning-border">Pending</span>
                                                        <!-- <span class="label label-danger-border">Unpaid</span> -->
                                                    </td>
                                                    <td>
                                                        <span class="label label-danger-border">Not Paid</span>
                                                        <!-- <span class="label label-danger-border">Unpaid</span> -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><a href="invoice-view.html">#INV-0003</a></td>
                                                    <td>
                                                        <h2><a href="#">Ben trav</a></h2>
                                                    </td>
                                                    <td>30 Nov 2017</td>
                                                    <td>28 Dec 2017</td>                                                    
                                                    <td>$380</td>
                                                    <td>
                                                        <span class="label label-success-border">Done</span>
                                                        <!-- <span class="label label-danger-border">Unpaid</span> -->
                                                    </td>
                                                    <td>
                                                        <span class="label label-success-border">Paid</span>
                                                        <!-- <span class="label label-danger-border">Unpaid</span> -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><a href="invoice-view.html">#INV-0004</a></td>
                                                    <td>
                                                        <h2><a href="#">Ben Dover</a></h2>
                                                    </td>
                                                    <td>4 Nov 2017</td>
                                                    <td>28 Dec 2017</td>                                                    
                                                    <td>$380</td>
                                                    <td>
                                                        <span class="label label-warning-border">Pending</span>
                                                        <!-- <span class="label label-danger-border">Unpaid</span> -->
                                                    </td>
                                                    <td>
                                                        <span class="label label-danger-border">Not Paid</span>
                                                        <!-- <span class="label label-danger-border">Unpaid</span> -->
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <a href="invoices.html" class="text-primary">View all packages being carried</a>
                                </div>
                            </div>
                        </div>                      
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-table">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Reported Carriers</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped custom-table m-b-0">
                                            <thead>
                                                <tr>
                                                    <th>Carrier ID</th>
                                                    <th>Carrier</th>
                                                    <th>Reporter/Sender Id</th>
                                                    <th>Reporter</th>
                                                    <th>Problem type</th>
                                                    <th>Problem status</th>
                                                    <th>Report Description</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><a href="invoice-view.html">#PER-0101</a></td>
                                                    <td>
                                                        <h2><a href="#">Hazel Nutt</a></h2>
                                                    </td>
                                                    <td><a href="invoice-view.html">#PER-0102</a></td>
                                                    <td>
                                                        <h2><a href="#">azel kutt</a></h2>
                                                    </td>
                                                    <td>
                                                        <h2><a href="#">Package Stolen</a></h2>
                                                        <!-- <span class="label label-danger-border">Sylo</span> -->
                                                        <!-- <span class="label label-danger-border">Unpaid</span> -->
                                                    </td>
                                                    <td>
                                                        <span class="label label-danger-border">Severe</span>
                                                        <!-- <span class="label label-danger-border">Unpaid</span> -->
                                                    </td>
                                                    <td>
                                                        <p>He has stolen the package #PKG-0121</p>
                                                    </td>
                                                    
                                                </tr>
                                                <tr>
                                                    <td><a href="invoice-view.html">#PER-0101</a></td>
                                                    <td>
                                                        <h2><a href="#">Hazel Nutt</a></h2>
                                                    </td>
                                                    <td><a href="invoice-view.html">#PER-0102</a></td>
                                                    <td>
                                                        <h2><a href="#">azel kutt</a></h2>
                                                    </td>
                                                    <td>
                                                        <h2><a href="#">Package in bad state</a></h2>
                                                        <!-- <span class="label label-danger-border">Sylo</span> -->
                                                        <!-- <span class="label label-danger-border">Unpaid</span> -->
                                                    </td>
                                                    <td>
                                                        <span class="label label-danger-border">Severe</span>
                                                        <!-- <span class="label label-danger-border">Unpaid</span> -->
                                                    </td>
                                                    <td>
                                                        <p>He has stolen the package #PKG-0121</p>
                                                    </td>
                                                    
                                                </tr>
                                                <tr>
                                                    <td><a href="invoice-view.html">#PER-0101</a></td>
                                                    <td>
                                                        <h2><a href="#">Hazel Nutt</a></h2>
                                                    </td>
                                                    <td><a href="invoice-view.html">#PER-0102</a></td>
                                                    <td>
                                                        <h2><a href="#">azel kutt</a></h2>
                                                    </td>
                                                    <td>
                                                        <h2><a href="#">Behaiviour Problem</a></h2>
                                                        <!-- <span class="label label-danger-border">Sylo</span> -->
                                                        <!-- <span class="label label-danger-border">Unpaid</span> -->
                                                    </td>
                                                    <td>
                                                        <span class="label label-danger-border">Severe</span>
                                                        <!-- <span class="label label-danger-border">Unpaid</span> -->
                                                    </td>
                                                    <td>
                                                        <p>He has stolen the package #PKG-0121</p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><a href="invoice-view.html">#PER-0101</a></td>
                                                    <td>
                                                        <h2><a href="#">Hazel Nutt</a></h2>
                                                    </td>
                                                    <td><a href="invoice-view.html">#PER-0102</a></td>
                                                    <td>
                                                        <h2><a href="#">azel kutt</a></h2>
                                                    </td>
                                                    <td>
                                                        <h2><a href="#">Behaiviour Problem</a></h2>
                                                        <!-- <span class="label label-danger-border">Sylo</span> -->
                                                        <!-- <span class="label label-danger-border">Unpaid</span> -->
                                                    </td>
                                                    <td>
                                                        <span class="label label-warning-border">Mediocore</span>
                                                        <!-- <span class="label label-danger-border">Unpaid</span> -->
                                                    </td>
                                                    <td>
                                                        <p>He has stolen the package #PKG-0121</p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <a href="clients.html" class="text-primary">View all Reported carriers</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                            
            </div>
@endsection
