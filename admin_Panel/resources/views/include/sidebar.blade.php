			<div class="sidebar" id="sidebar">
				<div class="sidebar-inner slimscroll">
					<div id="sidebar-menu" class="sidebar-menu">
						
						<ul >
							<li style="margin-top: 20px;">
								<div class="container">
									<div class="row" style="padding-left: 20px;">
										<div class="col-sm-2 ">
											<img src="assets/img/user-03.jpg" width="150" height="150" alt="" style="border-radius: 50%">
										</div>
									</div>
									<div class="row" style="padding-left: 20px;">
										<div class="col-sm-2">
											<h3>Aurnab Das</h3>
											<h5>Branch ID: {{Auth::user()->branch_id}}</h5>	
										</div>
									</div>
									<div class="row" style="padding-left: 20px;"> <!-- style="padding-left: 10px;text-align: center;"> -->
										<div class="col-sm-2">
											<h5>Admin</h5>
											<!-- <h3 class="glyphicon glyphicon-cog"></h3>	 -->
										</div>
									</div>
									<div class="row" style="padding-left: 20px;"> <!-- style="padding-left: 10px; text-align: center;"> -->
										<div class="col-sm-2">
											<h5>Bangladesh<br>
											Dhaka</h5>												
										</div>
									</div>
									
								</div>
							</li>
							<li  style="margin-top: 30px"> <!-- class="active" -->
								<a href="adminhome" class="noti-dot">Dashboard</a>
							</li>
							<li class="submenu">
								<a href="#" ><span> Request</span> <span class="menu-arrow"></span></a>
								<ul class="list-unstyled" style="display: none;">
									<li><a href="sendingpackages">Send</a></li>
									<li><a href="carryingpackages">Carry</a></li>
									
								</ul>
							</li>
							<li> 
								<a href="tracking">Tracking</a>
							</li>
							
							<li class="submenu">
								<a href="#"><span> Search</span> <span class="menu-arrow"></span></a>
								<ul class="list-unstyled" style="display: none;">
									<li><a href="searchsend">Send</a></li>
									<li><a href="searchcarry">Carry</a></li>
									<li><a href="users">Users</a></li>
								</ul>
							</li>
							<li class="submenu">
								<a href="#"><span> Packages </span> <span class="menu-arrow"></span></a>
								<ul class="list-unstyled" style="display: none;">
									<li><a href="ownbranchrecievingpackages"> Recieving packages </a></li>
									<li><a href="ownbranchsendingpackages"> Sending packages </a></li>
								</ul>
							</li>
							<li> 
								<a href="reports">Report</a>
							</li>
							<!-- <li> 
								<a href="leads">Product Claims</a>
							</li> -->
							<li> 
								<a href="settings">Settings</a>
							</li>
							<!-- <li class="submenu">
								<a href="#"><span> Pages </span> <span class="menu-arrow"></span></a>
								<ul class="list-unstyled" style="display: none;">
									<li><a href="login"> Login </a></li>
									<li><a href="register"> Register </a></li>
									<li><a href="forgot-password"> Forgot Password </a></li>
									<li><a href="profile"> Profile </a></li>
								</ul>
							</li> -->
						</ul>
					</div>
				</div>
			</div>