 @extends('layouts.insideapp')

<div class="page-wrapper">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-lg-3">
                            <div class="dash-widget clearfix card-box">
                                <span class="dash-widget-icon"><i class="fa fa-user" aria-hidden="true"></i></span>
                                <div class="dash-widget-info">
                                    <h3>{{App\users::totalusers()}}</h3>
                                    <span>Total Users</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-lg-3">
                            <div class="dash-widget clearfix card-box">
                                <span class="dash-widget-icon"><i class="fa fa-user" aria-hidden="true"></i></span>
                                <div class="dash-widget-info">
                                    <h3>14</h3>
                                    <span>Local Clients</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-lg-3">
                            <div class="dash-widget clearfix card-box">
                                <span class="dash-widget-icon"><i class="fa fa-cubes"></i></span>
                                <div class="dash-widget-info">
                                    <h3>14</h3>
                                    <span>Incoming packages</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-lg-3">
                            <div class="dash-widget clearfix card-box">
                                <span class="dash-widget-icon"><i class="fa fa-cubes" aria-hidden="true"></i></span>
                                <div class="dash-widget-info">
                                    <h3>14

                                    </h3>
                                    <span>Outgoing Packages</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-sm-6 text-center">
                                    <div class="card-box">
                                        <div id="area-chart" ></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 text-center">
                                    <div class="card-box">
                                        <div id="pie-chart" ></div>
                                    </div>
                                </div>
                                <div class=" col-sm-12 text-center">
                                    <div class="card-box">
                                        <div id="line-chart"></div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div style="text-align: center;background-color: #ffc857;"> <!-- //#a1bcc6 -->
                        <hr>
                        <h5>Branch ID: {{Auth::user()->branch_id}}</h5> 
                        <h4>Sending Packages</h4>
                        <hr>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="panel panel-table">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Home Pickup By Office</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped custom-table m-b-0">
                                            <thead>
                                                <tr>
                                                    <th>Package ID</th>
                                                    <th>Sender</th>
                                                    <th>Pickup Date</th>
                                                    <th>Total</th>
                                                    <th>Pickup Status</th>
                                                    <th>Carrier</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <form action="test" method="post" name= "form">
                                                    @foreach( $homep_package as $packag)
                                                    
                                                    <?    $i=0; ?>
                                                        @if($i==5)
                                                            <?break;?>

                                                        @else
                                                            <?$i++;?>
                                                            <tr>
                                                                <td><a href="/{{$packag->packageid}}" name= "{{ $packag->packageid }}">{{ $packag->packageid }}</a></td>
                                                                <td>
                                                                    <h2><a href="#">{{ $packag->sender_name }}</a></h2>
                                                                </td>
                                                                <td>{{ $packag->sending_date }}</td>
                                                                <td>{{ $packag->total_price }}</td>
                                                                <td>
                                                                    <span class="label label-warning-border">Pending</span>
                                                                    <!-- <span class="label label-danger-border">Unpaid</span> -->
                                                                </td>
                                                                <td>
                                                                    <span class="label label-success-border">@if($packag->allocated == 1)
                                                                    Allocated

                                                                    @else
                                                                        not allocated
                                                                    @endif
                                                                     </span>
                                                                    <!-- <span class="label label-danger-border">Unpaid</span> -->
                                                                </td>
                                                            </tr>
                                                        @endif
                                                    
                                                    @endforeach
                                                </form>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <a href="homepickup" class="text-primary">View all Pickup by office</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-table">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Delivery Packages To Office</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">  
                                        <table class="table table-striped custom-table m-b-0">
                                            <thead>
                                                <tr>
                                                    <th>Package ID</th>
                                                    <th>Sender</th>
                                                    <th>Delivery Date</th>
                                                    <th>Total</th>
                                                    <th>Delivery Status</th>
                                                    <th>Carrier</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach( $officed_package as $packag)
                                                    <?    $i=0; ?>
                                                        @if($i==5)
                                                            <?break;?>

                                                        @else
                                                            <?$i++;?>

                                                            <tr>
                                                                <td><a href="/{{$packag->packageid}}">{{ $packag->packageid }}</a></td>
                                                                <td>
                                                                    <h2><a href="#">{{ $packag->sender_name }}</a></h2>
                                                                </td>
                                                                <td>{{ $packag->sending_date }}</td>
                                                                <td>{{ $packag->total_price }}</td>
                                                                <td>
                                                                    <span class="label label-warning-border">
                                                                        @if($packag->completed == 0)
                                                                            Pending

                                                                        @else
                                                                            Success
                                                                        @endif
                                                                    </span>
                                                                    <!-- <span class="label label-danger-border">Unpaid</span> -->
                                                                </td>
                                                                <td>
                                                                    <span class="label label-success-border">@if($packag->carrier_id == NULL)
                                                                    not allocated

                                                                    @else
                                                                        Allocated
                                                                    @endif
                                                                     </span>
                                                                    <!-- <span class="label label-danger-border">Unpaid</span> -->
                                                                </td>
                                                            </tr>

                                                        @endif

                                                    
                                                    
                                                @endforeach



                                                
                                              
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <a href="deliverypackagetooffice" class="text-primary">View all Delivery packages to office</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-table">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Problem Packages</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped custom-table m-b-0">
                                            <thead>
                                                <tr>
                                                    <th>Package ID</th>
                                                    <th>Sender</th>
                                                    <th>Carrier</th>
                                                    <th>Sending Date</th>
                                                    <th>Recieving Date</th>
                                                    <th>Price</th>
                                                    <th>Problem Status</th>
                                                    <th class="text-right">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach( $problem_package as $packag)
                                                    <?    $i=0; ?>
                                                        @if($i==5)
                                                            <?break;?>

                                                        @else
                                                            <?$i++;?>
                                                            <tr>
                                                                <td><a href="/{{$packag->packageid}}">{{ $packag->packageid }}</a></td>
                                                                <td>
                                                                    <h2><a href="#">{{ $packag->sender_name }}</a></h2>
                                                                </td>
                                                                <td>
                                                                    <h2><a href="#">{{ $packag->carrier_name }} </a></h2>
                                                                </td>
                                                                <td>{{ $packag->sending_date }}</td>
                                                                <td>{{ $packag->arrival_date }}</td>
                                                                <td>{{ $packag->total_price }}</td>
                                                                
                                                                <td>
                                                                    <div class="dropdown action-label">
                                                                        <a class="btn btn-white btn-sm rounded dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-dot-circle-o text-danger"></i> 
                                                                            @if($packag->active == 0)
                                                                                Inactive

                                                                            @else
                                                                                Active
                                                                            @endif
                                                                             <i class="caret"></i>
                                                                        </a>
                                                                        <ul class="dropdown-menu pull-right">

                                                                            <li><a href="#"><i class="fa fa-dot-circle-o text-success"></i> Active</a></li>
                                                                            <li><a href="#"><i class="fa fa-dot-circle-o text-danger"></i> Inactive</a></li>
                                                                        </ul>
                                                                    </div>
                                                                </td>

                                                                <td class="text-right">
                                                                    <div class="dropdown">
                                                                        <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                                                                        <ul class="dropdown-menu pull-right">
                                                                            <li><a href="#" title="Edit"><i class="fa fa-pencil m-r-5"></i> Edit</a></li>
                                                                            <li><a href="#" title="Delete"><i class="fa fa-trash-o m-r-5"></i> Delete</a></li>
                                                                        </ul>
                                                                    </div>
                                                                </td>
                                                                
                                                            </tr>
                                                        @endif

                                                        
                                                    
                                                    
                                                @endforeach
  
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <a href="sendingpackagesproblem" class="text-primary">View all Problem Packages</a>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div style="text-align: center;background-color: #ffc857 ;"> <!-- //#a1bcc6 -->
                        <hr>
                        <h4>Carrying Packages</h4>
                        <hr>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-table">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Packages being carried</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped custom-table m-b-0">
                                            <thead>
                                                <tr>
                                                    <th>Package ID</th>
                                                    <th>Carrier</th>
                                                    <th>Pickup Date</th>
                                                    <th>Delevery Date</th>
                                                    <th>Total</th>
                                                    <th>Status</th>
                                                    <th>Payment Status </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                                @foreach( $carry_package as $packag)
                                                    <?    $i=0; ?>
                                                        @if($i==5)
                                                            <?break;?>

                                                        @else
                                                            <?$i++;?>
                                                            <tr>
                                                                <td><a href="/{{$packag->packageid}}">{{ $packag->packageid }}</a></td>
                                                                
                                                                <td>
                                                                    <h2><a href="#">{{ $packag->carrier_name }} </a></h2>
                                                                </td>
                                                                <td>{{ $packag->sending_date }}</td>
                                                                <td>{{ $packag->arrival_date }}</td>
                                                                <td>{{ $packag->total_price }}</td>
                                                                
                                                                <td>
                                                                    <div class="dropdown action-label">
                                                                        <a class="btn btn-white btn-sm rounded "  aria-expanded="false"><i class="fa fa-dot-circle-o text-warning"></i> 
                                                                            @if($packag->active == 0)
                                                                                Inacive

                                                                            @else
                                                                                Active
                                                                            @endif
                                                                             <i class="caret"></i>
                                                                        </a>
                                                                        
                                                                    </div>
                                                                </td>

                                                                <td>
                                                                            <span class="label label-warning-border">@if($packag->completed == 1)
                                                                            PAID

                                                                            @else
                                                                                NOT PAID
                                                                            @endif  
                                                                             </span>
                                                                            <!-- <span class="label label-danger-border">Unpaid</span> -->
                                                                        </td>
                                                                
                                                            </tr>
                                                        @endif

                                                        
                                                    
                                                    
                                                @endforeach





                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <a href="carryingpackages" class="text-primary">View all packages being carried</a>
                                </div>
                            </div>
                        </div>                      
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-table">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Reported Carriers</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped custom-table m-b-0">
                                            <thead>
                                                <tr>
                                                    <th>Carrier ID</th>
                                                    <th>Carrier</th>
                                                    <th>Reporter/Sender Id</th>
                                                    <th>Reporter</th>
                                                    <th>Problem type</th>
                                                    <th>Problem status</th>
                                                    <th>Report Description</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach( $report_carriers as $packag)
                                                    <?    $i=0; ?>
                                                        @if($i==5)
                                                            <?break;?>

                                                        @else
                                                            <?$i++;?>
                                                            <tr>
                                                                <td><a href="#">{{ $packag->carrier_id }}</a></td> <!-- ///{{$packag->packageid}} -->
                                                                
                                                                <td>
                                                                    <h2><a href="#">{{ $packag->carrier_name }} </a></h2>
                                                                </td>
                                                                <td><a href="#">{{ $packag->sender_id }}</a></td> <!-- /{{$packag->packageid}} -->
                                                                <td>
                                                                    <h2><a href="#">{{ $packag->sender_name }} </a></h2>
                                                                </td>
                                                                <td>{{ $packag->report_type }}</td>
                                                                
                                                                <td>
                                                                    <div class="dropdown action-label">
                                                                        <a class="btn btn-white btn-sm rounded "  aria-expanded="false"><i class="fa fa-dot-circle-o text-warning"></i> 
                                                                            @if($packag-> report_solved == 0)
                                                                                Pending

                                                                            @else
                                                                                Solved
                                                                            @endif
                                                                             <i class="caret"></i>
                                                                        </a>
                                                                        
                                                                    </div>
                                                                </td>

                                                                <td>{{ $packag->report_details }}</td>
                                                                
                                                            </tr>
                                                        @endif

                                                        
                                                    
                                                    
                                                @endforeach
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <a href="clients" class="text-primary">View all Reported carriers</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                            
            </div>