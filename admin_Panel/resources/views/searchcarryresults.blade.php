 @extends('layouts.insideapp')

<div class="page-wrapper">
                <div class="content container-fluid">
					<div class="row">
						<div class="col-sm-8">
							<h4 class="page-title">Search Results - Carrying Packages</h4>
						</div>
					</div>
					<div class="row filter-row">
						<div class="col-sm-3 col-md-2 col-xs-6">  
							<div class="form-group form-focus">
								<label class="control-label">Package Id</label>
								<input type="text" class="form-control floating" />
							</div>
					   </div>
						
						<div class="col-sm-3 col-xs-6">  
							<div class="form-group form-focus">
								<label class="control-label">From</label>
								<div class="cal-icon"><input class="form-control floating datetimepicker" type="text"></div>
							</div>
						</div>
						<div class="col-sm-3 col-xs-6">  
							<div class="form-group form-focus">
								<label class="control-label">To</label>
								<div class="cal-icon"><input class="form-control floating datetimepicker" type="text"></div>
							</div>
						</div>
						<!-- <div class="col-sm-3 col-xs-6"> 
							<div class="form-group form-focus select-focus">
								<label class="control-label">Status</label>
								<select class="select floating"> 
									<option value="">Select Status</option>
									<option value="">Pending</option>
									<option value="1">Paid</option>
									<option value="1">Partially Paid</option>
								</select>
							</div>
						</div> -->
						<div class="col-sm-3 col-xs-6">  
							<a href="#" class="btn btn-success btn-block"> Search </a>  
						</div>     
                    </div>
					<div class="row">
						<div class="col-md-12">
							<div class="table-responsive">
								<table class="table table-striped custom-table m-b-0 datatable">
									<thead>
										<tr>
											<th>#</th>
											<<th>Package ID</th>
											<th>Sender</th>
											<th>Pickup Date</th>
											<th>Total</th>
											<th>Pickup Status</th>
											<th>Carrier</th>
											<th>Created</th>
											<th class="text-right">Actions</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td><a href="invoice-view">#INV-0001</a></td>
											<td>
												<h2><a href="#">Hazel Nutt</a></h2>
											</td>
											<td>28 Dec 2017</td>
											<td>$380</td>
											<td>
												<span class="label label-warning-border">Pending</span>
												<!-- <span class="label label-danger-border">Unpaid</span> -->
											</td>
											<td>
												<span class="label label-warning-border">Not Allocated</span>
												<!-- <span class="label label-danger-border">Unpaid</span> -->
											</td>
											<td>10 hrs ago</td>
											<td class="text-right">
												<div class="dropdown">
													<a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
													<ul class="dropdown-menu pull-right">
														<li><a href="#"><i class="fa fa-pencil m-r-5"></i> Edit</a></li>
														<li><a href="#"><i class="fa fa-trash-o m-r-5"></i> Delete</a></li>
													</ul>
												</div>
											</td>
										</tr>
										<tr>
											<td>2</td>
											<td><a href="invoice-view">#INV-0002</a></td>
													<td>
														<h2><a href="#">curli Turner</a></h2>
													</td>
													<td>17 Dec 2017</td>
													<td>$500</td>
													<td>
														<span class="label label-warning-border">Pending</span>
													</td>
													<td>
														<span class="label label-warning-border">Not Allocated</span>
														<!-- <span class="label label-danger-border">Unpaid</span> -->
													</td>
											<td>5 Dec 2017</td>
											<td class="text-right">
												<div class="dropdown">
													<a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
													<ul class="dropdown-menu pull-right">
														<li><a href="#"><i class="fa fa-pencil m-r-5"></i> Edit</a></li>
														<li><a href="#"><i class="fa fa-trash-o m-r-5"></i> Delete</a></li>
													</ul>
												</div>
											</td>
										</tr>
										<tr>
											<td>3</td>
											<td><a href="invoice-view">#INV-0003</a></td>
													<td>
														<h2><a href="#">Ben trav</a></h2>
													</td>
													<td>30 Nov 2017</td>
													<td>$60</td>
													<td>
														
														<span class="label label-success-border">Done</span>
													</td>
													<td>
														<span class="label label-warning-border">Not Allocated</span>
														<!-- <span class="label label-danger-border">Unpaid</span> -->
													</td>
											<td>30 Nov 2017</td>
											<td class="text-right">
												<div class="dropdown">
													<a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
													<ul class="dropdown-menu pull-right">
														<li><a href="#"><i class="fa fa-pencil m-r-5"></i> Edit</a></li>
														<li><a href="#"><i class="fa fa-trash-o m-r-5"></i> Delete</a></li>
													</ul>
												</div>
											</td>
										</tr>
										<tr>
											<td>4</td>
											<td><a href="invoice-view">#INV-0004</a></td>
													<td>
														<h2><a href="#">Ben Dover</a></h2>
													</td>
													<td>4 Nov 2017</td>
													<td>$60</td>
													<td>
														
														<span class="label label-success-border">Done</span>
													</td>
													<td>
														<span class="label label-success-border">Allocated</span>
													</td>
											<td>10 hrs ago</td>
											<td class="text-right">
												<div class="dropdown">
													<a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
													<ul class="dropdown-menu pull-right">
														<li><a href="#"><i class="fa fa-pencil m-r-5"></i> Edit</a></li>
														<li><a href="#"><i class="fa fa-trash-o m-r-5"></i> Delete</a></li>
													</ul>
												</div>
											</td>
										</tr>
										<tr>
											<td>5</td>
											<td><a href="invoice-view">#INV-0001</a></td>
											<td>
												<h2><a href="#">Hazel Nutt</a></h2>
											</td>
											<td>28 Dec 2017</td>
											<td>$380</td>
											<td>
												<span class="label label-warning-border">Pending</span>
												<!-- <span class="label label-danger-border">Unpaid</span> -->
											</td>
											<td>
												<span class="label label-warning-border">Not Allocated</span>
												<!-- <span class="label label-danger-border">Unpaid</span> -->
											</td>
											<td>5 Dec 2017</td>
											<td class="text-right">
												<div class="dropdown">
													<a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
													<ul class="dropdown-menu pull-right">
														<li><a href="#"><i class="fa fa-pencil m-r-5"></i> Edit</a></li>
														<li><a href="#"><i class="fa fa-trash-o m-r-5"></i> Delete</a></li>
													</ul>
												</div>
											</td>
										</tr>
										<tr>
											<td>6</td>
											<td><a href="invoice-view">#INV-0001</a></td>
											<td>
												<h2><a href="#">Hazel Nutt</a></h2>
											</td>
											<td>28 Dec 2017</td>
											<td>$380</td>
											<td>
												<span class="label label-warning-border">Pending</span>
												<!-- <span class="label label-danger-border">Unpaid</span> -->
											</td>
											<td>
												<span class="label label-warning-border">Not Allocated</span>
												<!-- <span class="label label-danger-border">Unpaid</span> -->
											</td>
											<td>30 Nov 2017</td>
											<td class="text-right">
												<div class="dropdown">
													<a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
													<ul class="dropdown-menu pull-right">
														<li><a href="#"><i class="fa fa-pencil m-r-5"></i> Edit</a></li>
														<li><a href="#"><i class="fa fa-trash-o m-r-5"></i> Delete</a></li>
													</ul>
												</div>
											</td>
										</tr>
										<tr>
											<td>7</td>
											<td><a href="invoice-view">#INV-0001</a></td>
											<td>
												<h2><a href="#">Hal Nutt</a></h2>
											</td>
											<td>28 Dec 2017</td>
											<td>$380</td>
											<td>
												<span class="label label-warning-border">Pending</span>
												<!-- <span class="label label-danger-border">Unpaid</span> -->
											</td>
											<td>
												<span class="label label-warning-border">Not Allocated</span>
												<!-- <span class="label label-danger-border">Unpaid</span> -->
											</td>
											<td>10 hrs ago</td>
											<td class="text-right">
												<div class="dropdown">
													<a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
													<ul class="dropdown-menu pull-right">
														<li><a href="#"><i class="fa fa-pencil m-r-5"></i> Edit</a></li>
														<li><a href="#"><i class="fa fa-trash-o m-r-5"></i> Delete</a></li>
													</ul>
												</div>
											</td>
										</tr>
										<tr>
											<td>8</td>
											<td><a href="invoice-view">#INV-0001</a></td>
											<td>
												<h2><a href="#">Aze Nutt</a></h2>
											</td>
											<td>28 Dec 2017</td>
											<td>$380</td>
											<td>
												<span class="label label-warning-border">Pending</span>
												<!-- <span class="label label-danger-border">Unpaid</span> -->
											</td>
											<td>
												<span class="label label-warning-border">Not Allocated</span>
												<!-- <span class="label label-danger-border">Unpaid</span> -->
											</td>
											<td>5 Dec 2017</td>
											<td class="text-right">
												<div class="dropdown">
													<a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
													<ul class="dropdown-menu pull-right">
														<li><a href="#"><i class="fa fa-pencil m-r-5"></i> Edit</a></li>
														<li><a href="#"><i class="fa fa-trash-o m-r-5"></i> Delete</a></li>
													</ul>
												</div>
											</td>
										</tr>
										<tr>
											<td>9</td>
											<td><a href="invoice-view">#INV-0001</a></td>
											<td>
												<h2><a href="#">zel utt</a></h2>
											</td>
											<td>28 Dec 2017</td>
											<td>$380</td>
											<td>
												<span class="label label-warning-border">Pending</span>
												<!-- <span class="label label-danger-border">Unpaid</span> -->
											</td>
											<td>
												<span class="label label-warning-border">Not Allocated</span>
												<!-- <span class="label label-danger-border">Unpaid</span> -->
											</td>
											<td>30 Nov 2017</td>
											<td class="text-right">
												<div class="dropdown">
													<a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
													<ul class="dropdown-menu pull-right">
														<li><a href="#"><i class="fa fa-pencil m-r-5"></i> Edit</a></li>
														<li><a href="#"><i class="fa fa-trash-o m-r-5"></i> Delete</a></li>
													</ul>
												</div>
											</td>
										</tr>
										<tr>
											<td>10</td>
											<td><a href="invoice-view">#INV-0001</a></td>
											<td>
												<h2><a href="#">azel Nutt</a></h2>
											</td>
											<td>28 Dec 2017</td>
											<td>$380</td>
											<td>
												<span class="label label-warning-border">Pending</span>
												<!-- <span class="label label-danger-border">Unpaid</span> -->
											</td>
											<td>
												<span class="label label-warning-border">Not Allocated</span>
												<!-- <span class="label label-danger-border">Unpaid</span> -->
											</td>
											<td>10 hrs ago</td>
											<td class="text-right">
												<div class="dropdown">
													<a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
													<ul class="dropdown-menu pull-right">
														<li><a href="#"><i class="fa fa-pencil m-r-5"></i> Edit</a></li>
														<li><a href="#"><i class="fa fa-trash-o m-r-5"></i> Delete</a></li>
													</ul>
												</div>
											</td>
										</tr>
										<tr>
											<td>11</td>
											<td><a href="invoice-view">#INV-0001</a></td>
											<td>
												<h2><a href="#">Hael Nut</a></h2>
											</td>
											<td>28 Dec 2017</td>
											<td>$380</td>
											<td>
												<span class="label label-warning-border">Pending</span>
												<!-- <span class="label label-danger-border">Unpaid</span> -->
											</td>
											<td>
												<span class="label label-warning-border">Not Allocated</span>
												<!-- <span class="label label-danger-border">Unpaid</span> -->
											</td>
											<td>5 Dec 2017</td>
											<td class="text-right">
												<div class="dropdown">
													<a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
													<ul class="dropdown-menu pull-right">
														<li><a href="#"><i class="fa fa-pencil m-r-5"></i> Edit</a></li>
														<li><a href="#"><i class="fa fa-trash-o m-r-5"></i> Delete</a></li>
													</ul>
												</div>
											</td>
										</tr>
										<tr>
											<td>12</td>
											<td><a href="invoice-view">#INV-0001</a></td>
											<td>
												<h2><a href="#">azel Nutt</a></h2>
											</td>
											<td>28 Dec 2017</td>
											<td>$380</td>
											<td>
												<span class="label label-warning-border">Pending</span>
												<!-- <span class="label label-danger-border">Unpaid</span> -->
											</td>
											<td>
												<span class="label label-warning-border">Not Allocated</span>
												<!-- <span class="label label-danger-border">Unpaid</span> -->
											</td>
											<td>30 Nov 2017</td>
											<td class="text-right">
												<div class="dropdown">
													<a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
													<ul class="dropdown-menu pull-right">
														<li><a href="#"><i class="fa fa-pencil m-r-5"></i> Edit</a></li>
														<li><a href="#"><i class="fa fa-trash-o m-r-5"></i> Delete</a></li>
													</ul>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
                </div>
				
            </div>
        