 @extends('layouts.insideapp')

<div class="page-wrapper">
                <div class="content container-fluid">
					<div class="row">
						<div class="col-sm-8">
							<h4 class="page-title">Sending Package Problem</h4>
						</div>
					</div>
					<div class="row filter-row">
						<form action="" method="post" role="search">
							{{ csrf_field() }}
								
							<div class="col-sm-3 col-md-2 col-xs-6">  
								<div class="form-group form-focus">
									<label class="control-label">Package Id</label>
									<input type="text" class="form-control floating" />
								</div>
						   </div>
						
						

							<!-- <div class="col-sm-3 col-xs-6">  
								<div class="form-group form-focus">
									<label class="control-label">From</label>
									<div class="cal-icon"><input class="form-control floating datetimepicker" type="text"></div>
								</div>
							</div>
							<div class="col-sm-3 col-xs-6">  
								<div class="form-group form-focus">
									<label class="control-label">To</label>
									<div class="cal-icon"><input class="form-control floating datetimepicker" type="text"></div>
								</div>
							</div> -->
							<!-- <div class="col-sm-3 col-xs-6"> 
								<div class="form-group form-focus select-focus">
									<label class="control-label">Status</label>
									<select class="select floating"> 
										<option value="">Select Status</option>
										<option value="">Pending</option>
										<option value="1">Paid</option>
										<option value="1">Partially Paid</option>
									</select>
								</div>
							</div> -->
							<div class="col-sm-3 col-xs-6">  
								<button type="submit" class="btn btn-success btn-block" >
									<!-- <a href="trackingresults" > --> Search  <!-- </a>  --> 
								</button>
							</div> 
						</form>   
                    </div>
					<div class="row">
						<div class="col-md-12">
							<div class="table-responsive">
								<table class="table table-striped custom-table m-b-0 datatable">
									<thead>
										<tr>
											<th>Package ID</th>
                                            <th>Sender</th>
                                            <th>Carrier</th>
                                            <th>Sending Date</th>
                                            <th>Recieving Date</th>
                                            <th>Price</th>
                                            <th>Problem Status</th>
                                            <th class="text-right">Action</th>
										</tr>
									</thead>
									<tbody>
										@foreach( $problem_package as $packag)

                                                            <tr>
                                                                <td><a href="invoice-view">{{ $packag->packageid }}</a></td>
                                                                <td>
                                                                    <h2><a href="#">{{ $packag->sender_name }}</a></h2>
                                                                </td>
                                                                <td>
                                                                    <h2><a href="#">{{ $packag->carrier_name }} </a></h2>
                                                                </td>
                                                                <td>{{ $packag->sending_date }}</td>
                                                                <td>{{ $packag->arrival_date }}</td>
                                                                <td>{{ $packag->total_price }}</td>
                                                                
                                                                <td>
                                                                    <div class="dropdown action-label">
                                                                        <a class="btn btn-white btn-sm rounded dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-dot-circle-o text-danger"></i> 
                                                                            @if($packag->active == 0)
                                                                                Inactive

                                                                            @else
                                                                                Active
                                                                            @endif
                                                                             <i class="caret"></i>
                                                                        </a>
                                                                        <ul class="dropdown-menu pull-right">

                                                                            <li><a href="#"><i class="fa fa-dot-circle-o text-success"></i> Active</a></li>
                                                                            <li><a href="#"><i class="fa fa-dot-circle-o text-danger"></i> Inactive</a></li>
                                                                        </ul>
                                                                    </div>
                                                                </td>

                                                                <td class="text-right">
                                                                    <div class="dropdown">
                                                                        <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                                                                        <ul class="dropdown-menu pull-right">
                                                                            <li><a href="#" title="Edit"><i class="fa fa-pencil m-r-5"></i> Edit</a></li>
                                                                            <li><a href="#" title="Delete"><i class="fa fa-trash-o m-r-5"></i> Delete</a></li>
                                                                        </ul>
                                                                    </div>
                                                                </td>
                                                                
                                                            </tr>
                                                                                                            
                                                    
                                                @endforeach
										
									</tbody>
								</table>
							</div>
						</div>
					</div>
                </div>
				
            </div>
        