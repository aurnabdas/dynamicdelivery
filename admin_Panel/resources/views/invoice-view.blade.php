 @extends('layouts.insideapp')
@foreach( $task as $packag)
@foreach( $packa_rec_details as $pr)
@foreach( $packa_sen_details as $ps)
@foreach( $carrier_details as $c)
@foreach( $sender_details as $s)
<div class="page-wrapper">
                <div class="content container-fluid">
					<div class="row">
						<div class="col-sm-8">
							<h4 class="page-title">Invoice</h4>
						</div>
						<!-- <div class="col-sm-4 text-right m-b-30">
							<div class="btn-group btn-group-sm">
								<button class="btn btn-default">CSV</button>
								<button class="btn btn-default">PDF</button>
								<button class="btn btn-default"><i class="fa fa-print fa-lg"></i> Print</button>
							</div>
						</div> -->
					</div>

                                                                
                    <!-- need to be edited -->
                                                                
                                                              
                                                   

					<div class="row">
						<div class="col-md-12">
							<div class="panel">
								<div class="panel-body">
									<div class="row">
										<!-- <div class="row" style="padding-left: 20px;">
											<div class="col-sm-2 ">
													<img src="assets/img/user-03.jpg" width="150" height="150" alt="" style="border-radius: 50%">
											</div>
										</div> -->
										<div class="col-md-6 m-b-20">
											<img src="assets/img/user-03.jpg" width="150" height="150" alt="" style="border-radius: 50%"><hr>
											<!-- <img src="assets/img/logo2.png" class="m-b-20" alt="" style="width: 100px;"> -->
				 							<ul class="list-unstyled">
												<li><strong>Name: {{ $packag->sender_name }}</strong></li>
												<li><strong>Address: {{ $s->address }}</strong></li>
												<li><strong>Sherman Oaks, CA, 91403</strong></li>
												
											</ul>
										</div>
										<div class="col-md-6 m-b-20">
											<div class="invoice-details">
												<h3 class="text-uppercase">Package Id: #INV-0001</h3>
												<a href="reports-view"><h4 class="text-uppercase">Reported: No</h4></a>
												<ul class="list-unstyled">
													<li>Sending Date: <span>October 12, 2017</span></li>
													<li>Recieving date: <span>November 25, 2017</span></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="row">
										
										<!-- <div class="col-md-6 col-lg-9 m-b-20">
											<h5>Package Details</h5>
				 							<ul class="list-unstyled">
				 								<li>Package Sender: Plabon D</li>
												<li>Package Reciever: Barry Cuda</li>
												<li><span>Global Technologies</span></li>
												<li>5754 Airport Rd</li>
												<li>Coosada, AL, 36020</li>
												<li>United States</li>
												<li>888-777-6655</li>
												<li><a href="#">barrycuda@example.com</a></li>
											</ul>
										</div> -->
										<div class="col-md-6 col-lg-6 m-b-20">
											<span class="text-muted"><strong>Package Details:</strong></span><br><br>
											<ul class="list-unstyled invoice-payment-details">
												<li>Package Sender: <span>Plabon D</span></li>
												<li>Package Reciever: <span>Barry Cuda</span></li>
												<li>Sending Address: <span>London E1 8BF, >3 Goodman Street</span></li>
												<li>Recieving Address: <span>864 Quiet Valley Lane,Sherman Oaks, CA, 91403</span></li>
												<li>Flight ID: <span>KFH37784028476740</span></li>
												<li>Sending Airport: <span>London City Airport</span></li>
												<li>Recieving Airport: <span>Van Nuys Airport</span></li>
												<li>Tracking Number: <span>BPT4E</span></li>
												<br><br>
												<li><h5>Total : <span class="text-right">$8,750</span></h5></li>
											</ul>
										</div>
										<div class="col-md-6 col-lg-6 m-b-20">
											<span class="text-muted"><strong>Carrier Details:</strong></span><br><br>
											<ul class="list-unstyled invoice-payment-details">
												
												<li>Carrier: <span>Profit Bank Europe</span></li>
												<li>Address: <span> Villiers St, London WC2N 6NS, UK</span></li>
												<li>Flight Id: <span>KFH37784028476740</span></li>
												<br><br>
												<li><h5>Carrier End Total : <span class="text-right">$8,750</span></h5></li>
											</ul>
										</div>
									</div>
									<div class="table-responsive">
										<table class="table table-striped table-hover">
											<thead>
												<tr>
													<th>#</th>
													<th>ITEM</th>
													<th class="hidden-xs">DESCRIPTION</th>
													<th>UNIT Weight</th>
													<th>QUANTITY</th>
													<th>TOTAL</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>1</td>
													<td>Mobile Phone</td>
													<td class="hidden-xs">Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
													<td>170 g</td>
													<td>1</td>
													<td>$200</td>
												</tr>
												<tr>
													<td>2</td>
													<td>Laptop</td>
													<td class="hidden-xs">Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
													<td>2.3 kg</td>
													<td>1</td>
													<td>$175</td>
												</tr>
												<tr>
													<td>3</td>
													<td>Bag</td>
													<td class="hidden-xs">Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
													<td>300 g</td>
													<td>3</td>
													<td>$270</td>
												</tr>
												<tr>
													<td>4</td>
													<td>Envelop</td>
													<td class="hidden-xs">Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
													<td>20 g</td>
													<td>2</td>
													<td>$240</td>
												</tr>
												
											</tbody>
										</table>
									</div>
									<div>
										<div class="row invoice-payment">
											<div class="col-sm-7">
											</div>
											<div class="col-sm-5">
												<div class="m-b-20">
													<h6>Total due</h6>
													<div class="table-responsive no-border">
														<table class="table m-b-0">
															<tbody>
																<tr>
																	<th>Subtotal:</th>
																	<td class="text-right">$800</td>
																</tr>
																<tr>
																	<th>Tax Percent: </th>
																	<td class="text-right"><span class="text-regular">(25%)</span></td>
																</tr>
																<tr>
																	<th>Tax Total: </th>
																	<td class="text-right">$50</td>
																</tr>
																<tr>
																	<th>Total:</th>
																	<td class="text-right text-primary"><h5>$850</h5></td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
										<div class="invoice-info">
											<h5>Other information</h5>
											<p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sed dictum ligula, cursus blandit risus. Maecenas eget metus non tellus dignissim aliquam ut a ex. Maecenas sed vehicula dui, ac suscipit lacus. Sed finibus leo vitae lorem interdum, eu scelerisque tellus fermentum. Curabitur sit amet lacinia lorem. Nullam finibus pellentesque libero, eu finibus sapien interdum vel</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
                </div>
				
            </div>
@endforeach        
@endforeach 
@endforeach 
@endforeach 
@endforeach 