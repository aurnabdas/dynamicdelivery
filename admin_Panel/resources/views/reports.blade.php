 @extends('layouts.insideapp')

<div class="page-wrapper">
                <div class="content container-fluid">
					<div class="row">
						<div class="col-xs-8">
							<h4 class="page-title">Reports</h4>
						</div>
						
					</div>
					<div class="row filter-row">
						<form action="" method="post" role="search">
							{{ csrf_field() }}
								
							<div class="col-sm-3 col-md-2 col-xs-6">  
								<div class="form-group form-focus">
									<label class="control-label">Package Id</label>
									<input type="text" class="form-control floating" />
								</div>
						   </div>

							<div class="col-sm-3 col-xs-6">  
								<button type="submit" class="btn btn-success btn-block" >
									<!-- <a href="trackingresults" > --> Search  <!-- </a>  --> 
								</button>
							</div> 
						</form> 
                    </div>
					<div class="row">
						<div class="col-md-12">
							<div class="table-responsive">
								<table class="table table-striped custom-table m-b-0 datatable">
									<thead>
										<tr>
											<th>Package Id</th>
											<th>Carrier</th>
											<th>Sender</th>
											<th>Pickup Date</th>
											<th>Arrival Date</th>
											<th>Amount</th>
											<th>Report Details</th>
											<th>Report</th>
											
										</tr>
									</thead>
									<tbody>

										@foreach( $reported_packages as $packag)                                                    
                                                    
                                            <tr>
                                                <td><a href="invoice-view">{{ $packag->packageid }}</a></td>
                                                <td>
                                                    <h2><a href="#">{{ $packag->carrier_name }} </a></h2>
                                                 </td>
                                                <td>
                                                	@if($packag->sender_name == null)
		                                            	<span class="label label-warning-border">Not Allocated</span>
                                                    @else
                                                        <h2><a href="#">{{ $packag->sender_name }}</a></h2>
                                                    @endif
                                                    
                                                </td>
                                                <td>
                                                	@if($packag->sending_date == '')
                                                        <span class="label label-warning-border">Not Allocated</span>

                                               		@else
                                                        {{ $packag->sending_date }}
                                                    @endif
                                                </td>
                                                <td>					
                                 					@if($packag->arrival_date == '')
                                                        <span class="label label-warning-border">Not Allocated</span>

                                                    @else
                                                        {{ $packag->arrival_date }}
                                                    @endif
                                                </td>
                                                <td>{{ $packag->total_price }}$</td>
                                                <td>{{ $packag->report_details }}</td>
                                                <!-- <td>
                                                	 @if($packag->report_details == 0  )
                                                            <span class="label label-success-border">not reported</span>
                                         			@else
                                                            <span class="label label-warning-border">Pending</span>
                                                    @endif
                                                    
                                                </td> -->
                                                
                                                <td>
                                                	@if($packag->report == 1)
                                                        <span class="label label-danger-border">Reported</span>
                                             		@else
                                                        <span class="label label-success-border">Not reported</span>         
                                                    @endif
                                                </td>
                                            </tr>
                                                                                             
                                        @endforeach
										
									</tbody>
								</table>
							</div>
						</div>
					</div>
                </div>
				
            </div>
			
        