<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarriersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carriers', function (Blueprint $table) {
            $table->integer('id');
            $table->string('name');
            $table->string('username');
            $table->string('email')->unique();
            $table->string('address');
            $table->string('d_o_b');
            $table->string('contact_num');
            $table->string('nid');
            $table->string('passport_num');
            $table->string('departure_date');
            $table->string('departure_time');
            $table->string('departure_dest');
            $table->string('arrival_date');
            $table->string('arrival_time');
            $table->string('arrival_dest');
            $table->string('departure_airport');
            $table->string('arrival_airport');
            $table->string('branch_id_from');
            $table->string('branch_id_to');
            $table->integer('packageid');
            $table->integer('carrierid');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carriers');
    }
}
