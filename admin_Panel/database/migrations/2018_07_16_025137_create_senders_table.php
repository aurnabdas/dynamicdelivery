<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSendersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('senders', function (Blueprint $table) {
            
            $table->integer('id');
            $table->string('name');
            $table->string('username');
            $table->string('email')->unique();
            $table->string('address');
            $table->string('d_o_b');
            $table->string('contact_num');
            $table->string('nid');
            $table->string('passport_num');
            $table->integer('packageid');
            $table->integer('carrierid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('senders');
    }
}
