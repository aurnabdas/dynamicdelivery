<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->increments('packageid');
            $table->string('total_items');
            $table->string('total_price');
            $table->string('tax_price');
            $table->string('taxable_product');
            $table->string('sender_id');
            $table->string('sender_name');
            $table->string('sending_dest');
            $table->string('recieving_dest');
            $table->string('total_num_carriers');
            $table->string('carrier_id');
            $table->string('carrier_name');
            $table->string('carrier_amount');
            $table->string('carrier_pickup_dest');
            $table->string('carrier_drop_dest');
            $table->string('package_route');
            $table->string('home_pickup');
            $table->string('office_drop');
            $table->string('branch_id_from');
            $table->string('branch_id_to');
            $table->boolean('allocated');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
