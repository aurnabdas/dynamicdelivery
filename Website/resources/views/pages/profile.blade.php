@extends('profile_layout')
@section('pplayout')



<div class="page-wrapper">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-sm-8">
                            <h4 class="page-title">My Profile</h4>
                        </div>
                        
                        <div class="col-sm-4 text-right m-b-30">
                            <a href="{{URL::to('/editprofile')}}" class="btn btn-primary rounded"><i class="fa fa-plus"></i> Edit Profile</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel">
                                <div class="panel-body">
                                    <div class="row">
                                        <!-- <div class="row" style="padding-left: 20px;">
                                            <div class="col-sm-2 ">
                                                    <img src="assets-2/img/user-03.jpg" width="150" height="150" alt="" style="border-radius: 50%">
                                            </div>
                                        </div> -->
                                        <div class="col-md-6 m-b-20">
                                            <img src="" width="150" height="150" alt="" style="border-radius: 50%"><hr>
                                            <!-- <img src="assets-2/img/logo2.png" class="m-b-20" alt="" style="width: 100px;"> -->
                                            <div class="invoice-info">
                                            <h5 class="title">Status</h5>
                                            <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sed dictum ligula, cursus blandit risus. Maecenas eget metus non tellus dignissim aliquam ut a ex. Maecenas sed vehicula dui, ac suscipit lacus. Sed finibus leo vitae lorem interdum, eu scelerisque tellus fermentum. Curabitur sit amet lacinia lorem. Nullam finibus pellentesque libero, eu finibus sapien interdum vel</p>
                                        </div>
                                            <ul class="list-unstyled">
                                                <li><strong>Name: {{$user->name}}</strong></li>
                                                <li><strong>Address: {{$sender->address}}</strong></li>
                                                  <li><strong>Gender: {{$user->gender}}</strong></li>
                                                <li><strong>Date of Bitrh: {{$user->date_of_birth}}</strong></li>
                                                
                                            </ul>
                                        </div>
                                        
                                    </div>
                                    <div class="row">
        
                                        <div class="col-md-6 col-lg-6 m-b-20">
                                            
                                            <ul class="list-unstyled invoice-payment-details">
                                                <li>Email Address : <span>{{$user->email}}</span></li>
                                                <li>Phone Number : <span>{{$sender->contact_num}}</span></li>
                                                
                                                
                                                <li>National ID Number: <span>{{$sender->address}}</span></li>
                                                <li>Passport Number : <span>{{$sender->passport_num}}</span></li>
                                                <li>Account Number : <span>{{$sender->account_num}}</span></li>
                                                
                                                <br><br>
                                                
                                            </ul>
                                        </div>
                                        
                                    </div>
                                
                                    <div>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>

            @endsection