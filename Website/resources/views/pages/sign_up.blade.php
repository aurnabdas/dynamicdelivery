  
@extends('f')
@section('frontpages')



<body id="home">
  <main class="wrapper">

            <!-- Header -->
            <header class="header-main">

                <!-- Header Topbar -->
                <div class="top-bar font2-title1 white-clr">
                    <div class="theme-container container">
                        <div class="row">
                            <div class="col-md-6 col-sm-5">
                                <ul class="list-items fs-10">
                                    <li><a href="#"></a></li>
                                    <li class="active"><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                </ul>
                            </div>
                            <div class="col-md-6 col-sm-7 fs-12">
                                <p class="contact-num">  <i class="fa fa-phone"></i> Call us now: <span class="theme-clr"> +880-1756-390-370 </span> </p>
                            </div>
                        </div>
                    </div>
                    <a data-toggle="modal" href="#login-popup" class="sign-in fs-12 theme-clr-bg"> sign in </a>
                </div>
                <!-- /.Header Topbar -->

                <!-- Header Logo & Navigation -->
                <nav class="menu-bar font2-title1">
                    <div class="theme-container container">
                        <div class="row">
                            <div class="col-md-2 col-sm-2">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-controls="navbar">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-logo" href=""> <img src="{{('assets/img/logo/logo-black.png')}}" alt="logo" /> </a>
                            </div>
                            <div class="col-md-10 col-sm-10 fs-12">
                                <div id="navbar" class="collapse navbar-collapse no-pad">
                                    <ul class="navbar-nav theme-menu">
                                        <li class="dropdown">
                                            <a href="{{URL::to('/')}}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" >Home </a>
                                           
                                        </li>
                                        <li> <a href="{{URL::to('/about_us')}}">about</a> </li>
                                       <!--  <li class="active"> <a href="{{URL::to('/usertracking')}}"> tracking </a> </li>
                                        <li> <a href="{{URL::to('/pricing_plans')}}"> pricing </a> </li> -->
                                        <li> <a href="{{URL::to('/contact_us')}}"> contact </a> </li>
                                        <li class="dropdown">
                                            <a href="{{URL::to('/sign_up')}}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" >sign up</a>
                                           
                                        </li>
                                         
                                        <li><span class="search fa fa-search theme-clr transition"> </span></li>
                                    </ul>                                                      
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
                <!-- /.Header Logo & Navigation -->

            </header>
            <!-- /.Header -->






                      <div class="title-wrap text-center  pb-50">
                            <h2 class="section-title wow fadeInUp" data-wow-offset="50" data-wow-delay=".20s">Use one Of this site to sign up</h2>
                        </div>


                         <div class="row">
                             <div style="text-align: center;">
                                 <div class="col-md-12 col-sm-12">
                              
                                <ul class="social-icons list-inline">
                                    <li class="wow fadeIn" data-wow-offset="50" data-wow-delay=".20s"> <a href="#" class="fa fa-facebook"></a>Facebook  </li>
                                    <li class="wow fadeIn" data-wow-offset="50" data-wow-delay=".25s"> <a href="#" class="fa fa-twitter"></a>Twitter </li>
                                    <li class="wow fadeIn" data-wow-offset="50" data-wow-delay=".30s"> <a href="#" class="fa fa-google-plus"></a> Google-plus</li>
                                    <li class="wow fadeIn" data-wow-offset="50" data-wow-delay=".35s"> <a href="#" class="fa fa-linkedin"></a>Linkedin </li>
                                </ul>
                               
                            </div>
                             </div>
                         </div>

                         <div class="row " >
                             <div class="col-sm-12 col-sm-offset-2" >
                                 <p >* Indicate Required Filed</p>
                             </div>
                         </div>


<div class="container">
    <div class="row">
 


<form method="POST" action="{{url('/sign_up')}}">
 
 {{  csrf_field()  }}
  
    <div class="form-horizontal" >


        <div class="form-group wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s">
        <label class="control-label title-2  col-sm-2 "  >First Name: *</label>
       <div class="col-sm-4">
            <input class="form-control" type="text" id="inputusername"  name="first_name" >
       </div>
        <label class="control-label title-2  col-sm-2 "  >Last Name: *</label>
       <div class="col-sm-4">
            <input class="form-control" type="text" id="inputusername" name="last_name">
       </div>
   </div>
    </div>

 <div class="form-horizontal" >


        <div class="form-group wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s">
         <label class="control-label title-2  col-sm-2 "  >gender : *</label>
           <div class="col-sm-4">

                                            <div class="form-group">
                                              
                                                <select name="gender" class="form-control" id="exampleFormControlSelect1">
                                                          <option value="Male">Male</option>
                                                          <option value="Female">Female</option>
                                                         
                                                          
                                                 </select>
                                            </div>
                                        </div>
        <label class="control-label title-2  col-sm-2 "  > Date of Birth: *</label>
       <div class="col-sm-4">
            <input class="form-control" type="date" id="inputusername" name="date_of_birth">
       </div>
   </div>
    </div>


     <div class="form-horizontal" >
        <div class="form-group wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s">
        <label class="control-label title-2  col-sm-2 "  >Passport Number : *</label>
       <div class="col-sm-3">
            <input class="form-control" type="text" id="inputusername" name="passport_num"> 
       </div>
        <label class="control-label title-2  col-sm-3 "  >National ID Number: *</label>
       <div class="col-sm-4">
            <input class="form-control" type="text" id="inputusername" name="nid">
       </div>
   </div>
    </div>

  


       <div class="form-horizontal" >
       <div class="form-group wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s"  >
            <label class="control-label title-2  col-sm-2 "  >Address: *</label>
       <div class="col-sm-10">
            <input class="form-control" type="text" id="inputusername" name="address">
       </div>
       </div>
    </div>

    
      <div class="form-horizontal" >
       <div class="form-group wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s"  >
            <label class="control-label title-2  col-sm-2 "  ></label>
       <div class="col-sm-10">
            <input class="form-control" type="" id="inputusername">
       </div>
       </div>
    </div>
     <div class="form-horizontal" >
       <div class="form-group wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s"   >
            <label class="control-label title-2  col-sm-2 "  ></label>
       <div class="col-sm-10">
            <input class="form-control" type="" id="inputusername">
       </div>
       </div>
    </div>


   

     
    <div class="form-horizontal" >
        <div class="form-group wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s">
        <label class="control-label title-2  col-sm-2 "  >Email: *</label>
       <div class="col-sm-4">
            <input class="form-control" type="text" id="inputusername" name="email">
       </div>
        <label class="control-label title-2  col-sm-2 "  >Contact Number: *</label>
       <div class="col-sm-4">
            <input class="form-control" type="text" id="inputusername" name="contact_num">
       </div>
   </div>
    </div>

    
     <div class="form-horizontal" >
       <div class="form-group wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s" >
            <label class="control-label title-2  col-sm-2 "  >Account Number : *</label>
       <div class="col-sm-8">
            <input class="form-control" type="text" id="inputusername" name="account_num">
       </div>
       </div>
    </div>


<div class="form-horizontal" >
        <div class="form-group wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s">
        <label class="control-label title-2  col-sm-2 "  >Password: *</label>
       <div class="col-sm-3">
            <input class="form-control" type="password" id="inputusername" name="password">
       </div>
   </div>
    </div>




   <div class="form-group">
    <div class="form-group wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s" >
        <div class="col-sm-2 col-sm-offset-2 custombtn " >
          <button name="submit" id="submit_btn" class="btn btn-success title-1"> Submit </button>
    </div>
    </div>
       
   </div>
  
  


</form>

</div>
</div>



@endsection