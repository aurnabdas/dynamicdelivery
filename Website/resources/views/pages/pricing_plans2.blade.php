    
  @extends('welcome')
  @section('content')


  <body id="home">
    <main class="wrapper">

              <!-- Header -->
              <header class="header-main">

                  <!-- Header Topbar -->
                  <div class="top-bar font2-title1 white-clr">
                      <div class="theme-container container">
                          <div class="row">
                              <div class="col-md-6 col-sm-5">
                                  <ul class="list-items fs-10">
                                      <li><a href="#"></a></li>
                                      <li class="active"><a href="#"></a></li>
                                      <li><a href="#"></a></li>
                                  </ul>
                              </div>
                              <div class="col-md-6 col-sm-7 fs-12">
                                  <p class="contact-num">  <i class="fa fa-phone"></i> Call us now: <span class="theme-clr"> +880-1756-390-370 </span> </p>
                              </div>
                          </div>
                      </div>
                      <a data-toggle="modal" href="#login-popup" class="sign-in fs-12 theme-clr-bg"> sign in </a>
                  </div>
                  <!-- /.Header Topbar -->

                  <!-- Header Logo & Navigation -->
                  <nav class="menu-bar font2-title1">
                      <div class="theme-container container">
                          <div class="row">
                              <div class="col-md-2 col-sm-2">
                                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-controls="navbar">
                                      <span class="sr-only">Toggle navigation</span>
                                      <span class="icon-bar"></span>
                                      <span class="icon-bar"></span>
                                      <span class="icon-bar"></span>
                                  </button>
                                  <a class="navbar-logo" href=""> <img src="{{('assets/img/logo/logo-black.png')}}" alt="logo" /> </a>
                              </div>
                              <div class="col-md-10 col-sm-10 fs-12">
                                  <div id="navbar" class="collapse navbar-collapse no-pad">
                                      <ul class="navbar-nav theme-menu">
                                          <li class="dropdown">
                                              <a href="{{URL::to('/')}}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" >Home </a>
                                             
                                          </li>
                                          <li> <a href="{{URL::to('/about_us')}}">about</a> </li>
                                          <li class="active"> <a href="{{URL::to('/usertracking')}}"> tracking </a> </li>
                                          <li> <a href="{{URL::to('/pricing_plans')}}"> pricing </a> </li>
                                          <li> <a href="{{URL::to('/contact_us')}}"> contact </a> </li>
                                          <li class="dropdown">
                                              <a href="{{URL::to('/sign_up')}}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" >sign up</a>
                                             
                                          </li>
                                           
                                          <li><span class="search fa fa-search theme-clr transition"> </span></li>
                                      </ul>                                                      
                                  </div>
                              </div>
                          </div>
                      </div>
                  </nav>
                  <!-- /.Header Logo & Navigation -->

              </header>
              <!-- /.Header -->



              
  <div class="work">
      <div class="step">
      <ul class="progressbar" >
          <li class="active title-1 " >Where </li>
          <li class="title-1" >what</li>
          <li class="title-1" >how</li>
          <li class="title-1">Details</li>
          <li class="title-1">Payment</li>
          <li class="title-1">Review</li>
          
      </ul>
      
  </div>
  </div>



      <div class="title-wrap text-center  pb-50">
        <h2 class="section-title wow fadeInUp" data-wow-offset="50" data-wow-delay=".20s">size and package type of the product </h2>
      </div>

<form method="POST" action="{{url('/pricing_plans2')}}">
  {{  csrf_field()  }}

  <div class="panel panel-default" style="max-width:1000px;margin-left:auto;margin-right:auto;">
    <div>
        <div class="panel-heading">
          <h3 class="panel-title text-center title-2 workofpanel ">Item 1</h3>
        </div>
        <div class="panel-body">          
            <div class="container">
              <div class="row">
                 

                  <div class="form-horizontal" >
                        <div class="form-group wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s">
                            <label class="control-label title-2  col-sm-2 "  >Item type:</label>
                           <div class="col-sm-8">
                                <input class="form-control" type="text" id="inputusername" name="type"  >
                           </div>
                        </div>
                  </div>         

                  <div class="form-horizontal" >
                        <div class="form-group wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s">
                            <label class="control-label title-2  col-sm-2 "  >weight:</label>
                           <div class="col-sm-3">
                                <input class="form-control" type="text" id="inputusername" name="weight">
                           </div>
                            <label class="control-label title-2  col-sm-2 "  >length:</label>
                           <div class="col-sm-3">
                                <input class="form-control" type="text" id="inputusername" name="length">
                           </div>
                           
                        </div>

                  </div>

                  <div class="form-horizontal" >
                    <label class="control-label title-2  col-sm-2 "  >width:</label>
                           <div class="col-sm-3">
                                <input class="form-control" type="text" id="inputusername" name="width">
                           </div>
                         <div class="form-group wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s"  >
                              <label class="control-label title-2  col-sm-2 "  >decleared value:</label>
                               <div class="col-sm-3">
                                    <input class="form-control" type="text" id="inputusername" name="decleared_value">
                               </div>
                         </div>
                  </div>    

              </div>
            </div>

            
        </div> 
    </div>

    <div class="newitem">
      
    </div>

    <div class="form-horizontal" >
        <input type="hidden"  class="control-label title-2  col-sm-2 "  name=" total" value="0" id="total"></label>
      </div>

    <!-- <div class="totalitem">
      
    </div> -->

      <div class="panel-body">          
        <div class="container">
          <div class="row">
              <div >
           <div id="accordion" role="tablist" aria-multiselectable="true">
                <div class="card">

               
                
                    <div class="card-header" role="tab" id="headingThree">
                      <h5 class="mb-0">
                        <a class="collapsed title-2" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                          <button class="btn btn-primary" type="submit" name="newitem" onclick="newitemadd()" >Add another item</button>
                        </a>
                      </h5>
                    </div>
                    
                </div>
              <div class="form-group">
                    <div class="form-group wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s" >
                        <div class="col-sm-1 col-sm-offset-2 custombtn " >
                          <button name="submit" id="submit_btn" class="btn btn-success title-1"> Continue</button>
                        </div>
                    </div>                     
              </div>               
           </div>
            <div class="form-group">
              <div class="form-group wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s" >
                  <div class="col-sm-2 col-sm-offset-2 custombtn " >
                    <button name="submit" id="submit_btn" class="btn btn-info title-1"> Back </button>
                  </div>
              </div>               
            </div>
         
           <div class="form-group">
                <div class="form-group wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s" >
                    <div class="col-sm-2 custombtn" >
                      <button class="btn btn-danger title-1" name="submit" id="submit_btn" > cancel shipment</button>
                    </div>
                </div>                
            </div>
              </div>
          </div>
        </div>
      </div>
  </div>

</form>


  <script>
    var i=0;
  // Example starter JavaScript for disabling form submissions if there are invalid fields

  function newitemadd(){
    i++;

    var text1=" <div class=\"panel-heading\">          <h3 class=\"panel-title text-center title-2 workofpanel \">Item "+(i+1)+"</h3>        </div> ";

    var text2="<div class=\"panel-body\">  <div class=\"container\">  <div class=\"row\">";

    var text3="<div class=\"form-horizontal\" >                       <div class=\"form-group wow fadeInUp\" data-wow-offset=\"50\" data-wow-delay=\".30s\">                            <label class=\"control-label title-2  col-sm-2 \"  >Item type:</label>                           <div class=\"col-sm-8\">                                <input class=\"form-control\" type=\"text\" id=\"inputusername\" name=\"type"+i+"\"  >                           </div>                        </div>                  </div> ";

    var text4="<div class=\"form-horizontal\" >                        <div class=\"form-group wow fadeInUp\" data-wow-offset=\"50\" data-wow-delay=\".30s\">                            <label class=\"control-label title-2  col-sm-2 \"  >weight:</label>                           <div class=\"col-sm-3\">                                <input class=\"form-control\" type=\"text\" id=\"inputusername\" name=\"weight"+i+"\">                           </div>                            <label class=\"control-label title-2  col-sm-2 \"  >length:</label>                           <div class=\"col-sm-3\">                                <input class=\"form-control\" type=\"text\" id=\"inputusername\" name=\"length"+i+"\">                           </div>                        </div>                  </div>";

    var text5=" <div class=\"form-horizontal\" >   <label class=\"control-label title-2  col-sm-2 \"  >width:</label>                           <div class=\"col-sm-3\">                                <input class=\"form-control\" type=\"text\" id=\"inputusername\" name=\"width"+i+"\">                           </div>                       <div class=\"form-group wow fadeInUp\" data-wow-offset=\"50\" data-wow-delay=\".30s\"  >                              <label class=\"control-label title-2  col-sm-2 \"  >decleared value:</label>                               <div class=\"col-sm-3\">                                    <input class=\"form-control\" type=\"text\" id=\"inputusername\" name=\"decleared_value"+i+"\">                               </div>                         </div>                  </div> ";

    var text6="</div>            </div>                    </div>";

   /* var texttotal="<div class=\"form-horizontal\" >                                                   <label class=\"control-label title-2  col-sm-2 \"  name=\" total\" value="+(i+1)+"></label>                                             </div>";
                  
*/
  var x = document.getElementById("total");
  x.setAttribute("value", (i+1));

    //$("div.totalitem").append(texttotal);
    $("div.newitem").append(text1,text2,text3,text4,text5,text6);
  }
  </script>



  @endsection