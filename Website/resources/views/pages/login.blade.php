@extends('welcome')
@section('content')

<body id="home">
    <main class="wrapper">

            <!-- Header -->
            <header class="header-main">

                <!-- Header Topbar -->
                <div class="top-bar font2-title1 white-clr">
                    <div class="theme-container container">
                        <div class="row">
                            <div class="col-md-6 col-sm-5">
                                <ul class="list-items fs-10">
                                    <li><a href="#"></a></li>
                                    <li class="active"><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                </ul>
                            </div>
                            <div class="col-md-6 col-sm-7 fs-12">
                                <p class="contact-num">  <i class="fa fa-phone"></i> Call us now: <span class="theme-clr"> +880-1756-390-370 </span> </p>
                            </div>
                        </div>
                    </div>
                    <a data-toggle="modal" href="#login-popup" class="sign-in fs-12 theme-clr-bg"> sign in </a>
                </div>
                <!-- /.Header Topbar -->

                <!-- Header Logo & Navigation -->
                <nav class="menu-bar font2-title1">
                    <div class="theme-container container">
                        <div class="row">
                            <div class="col-md-2 col-sm-2">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-controls="navbar">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-logo" href=""> <img src="{{('assets/img/logo/logo-black.png')}}" alt="logo" /> </a>
                            </div>
                            <div class="col-md-10 col-sm-10 fs-12">
                                <div id="navbar" class="collapse navbar-collapse no-pad">
                                    <ul class="navbar-nav theme-menu">
                                        <li class="dropdown">
                                            <a href="{{URL::to('/')}}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" >Home </a>
                                           
                                        </li>
                                        <li> <a href="{{URL::to('/about_us')}}">about</a> </li>
                                        <li class="active"> <a href="{{URL::to('/usertracking')}}"> tracking </a> </li>
                                        <li> <a href="{{URL::to('/pricing_plans')}}"> pricing </a> </li>
                                        <li> <a href="{{URL::to('/contact_us')}}"> contact </a> </li>
                                        <li class="dropdown">
                                            <a href="{{URL::to('/sign_up')}}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" >sign up</a>
                                           
                                        </li>
                                         
                                        <li><span class="search fa fa-search theme-clr transition"> </span></li>
                                    </ul>                                                      
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
                <!-- /.Header Logo & Navigation -->

            </header>
            <!-- /.Header -->


            
                      <div class="col-sm-10  col-sm-offset-1">

    
<h1 class="control-label">New to Share And Earn ? <span class="control-label "><a href="#"> Sign Up</a></span> </h1>

   
    
</div>
                 <div class="col-sm-10  col-sm-offset-1">

    
<h1 class="control-label ">use One Of This Site </h1>

   
    
</div>


                         <div class="row">
                             <div class="col-sm-offset-2" >
                                 <div class="col-md-12 col-sm-12">
                              
                                <ul class="social-icons list-inline">
                                    <li class="wow fadeIn" data-wow-offset="50" data-wow-delay=".20s"> <a href="#" class="fa fa-facebook"></a>Facebook  </li>
                                    <li class="wow fadeIn" data-wow-offset="50" data-wow-delay=".25s"> <a href="#" class="fa fa-twitter"></a>Twitter </li>
                                    <li class="wow fadeIn" data-wow-offset="50" data-wow-delay=".30s"> <a href="#" class="fa fa-google-plus"></a> Google-plus</li>
                                    <li class="wow fadeIn" data-wow-offset="50" data-wow-delay=".35s"> <a href="#" class="fa fa-linkedin"></a>Linkedin </li>
                                </ul>
                               
                            </div>
                             </div>
                         </div>

                         <div class="row " >
                             <div class="col-sm-12 col-sm-offset-1" >
                                 <p >* Indicate Required Filed</p>
                             </div>
                         </div>
                                    <div class="col-sm-10  col-sm-offset-1">
                                     
    
                                        <h1 class="control-label ">

                                            <?php

                                                   $message=Session::get('message');
                                                   if ($message) {
                                                       # code...
                                                    echo $message;
                                                    Session::put('null');
                                                   }
                                            ?>

                                         </h1>

   
    
                                    </div>


<div class="container">
    <div class="row">
       
<form method="POST" action="{{url('/login')}}"  >
  {{  csrf_field() }}
   
    
                                <div class="form-group col-sm-7">
                                  <label class="control-label title-2 "  >Email or user Id  *</label>
                                    <input type="text" placeholder="" class="form-control" required="Email"  name="email" >
                                </div>
                               <div class="form-group col-sm-7">
                                  <label class="control-label title-2 "  >Password * </label>
                                    <input type="Password" placeholder="" class="form-control" required="enter the Password" name="Password"></div>
                                <div class="form-group">
                                    
                                </div>
                            
                            <div class="col-sm-5 col-sm-offset-5" >
                                <a href="#" class="gray-clr"> Forgot Passoword? </a>
                            </div>                            
                    








   <div class="form-group">
    <div class="form-group wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s" >
        <div class="col-sm-2 col-sm-offset-1 custombtn " >
          <button name="submit" id="submit_btn" class="btn btn-success title-1"> Submit </button>
    </div>
    </div>
       
   </div>
  
  


</form>

</div>
</div>




@endsection