  
@extends('welcome')
@section('content')


<body id="home">
	<main class="wrapper">

            <!-- Header -->
            <header class="header-main">

                <!-- Header Topbar -->
                <div class="top-bar font2-title1 white-clr">
                    <div class="theme-container container">
                        <div class="row">
                            <div class="col-md-6 col-sm-5">
                                <ul class="list-items fs-10">
                                    <li><a href="#"></a></li>
                                    <li class="active"><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                </ul>
                            </div>
                            <div class="col-md-6 col-sm-7 fs-12">
                                <p class="contact-num">  <i class="fa fa-phone"></i> Call us now: <span class="theme-clr"> +880-1756-390-370 </span> </p>
                            </div>
                        </div>
                    </div>
                    <a data-toggle="modal" href="#login-popup" class="sign-in fs-12 theme-clr-bg"> sign in </a>
                </div>
                <!-- /.Header Topbar -->

                <!-- Header Logo & Navigation -->
                <nav class="menu-bar font2-title1">
                    <div class="theme-container container">
                        <div class="row">
                            <div class="col-md-2 col-sm-2">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-controls="navbar">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-logo" href=""> <img src="{{('assets/img/logo/logo-black.png')}}" alt="logo" /> </a>
                            </div>
                            <div class="col-md-10 col-sm-10 fs-12">
                                <div id="navbar" class="collapse navbar-collapse no-pad">
                                    <ul class="navbar-nav theme-menu">
                                        <li class="dropdown">
                                            <a href="{{URL::to('/')}}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" >Home </a>
                                           
                                        </li>
                                        <li> <a href="{{URL::to('/about_us')}}">about</a> </li>
                                        <li class="active"> <a href="{{URL::to('/usertracking')}}"> tracking </a> </li>
                                        <li> <a href="{{URL::to('/pricing_plans')}}"> pricing </a> </li>
                                        <li> <a href="{{URL::to('/contact_us')}}"> contact </a> </li>
                                        <li class="dropdown">
                                            <a href="{{URL::to('/sign_up')}}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" >sign up</a>
                                           
                                        </li>
                                         
                                        <li><span class="search fa fa-search theme-clr transition"> </span></li>
                                    </ul>                                                      
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
                <!-- /.Header Logo & Navigation -->

            </header>
            <!-- /.Header -->



            
<div class="work">
    <div class="step">
    <ul class="progressbar" >
        <li class="active title-1 " >Where </li>
        <li class="title-1" >what</li>
        <li class="title-1" >how</li>
        <li class="title-1">Details</li>
        <li class="title-1">Payment</li>
        <li class="title-1">Review</li>
        
    </ul>
    
</div>
</div>












                      <div class="title-wrap text-center  pb-50">
                            <h2 class="section-title wow fadeInUp underline-small2" data-wow-offset="50" data-wow-delay=".20s">How Would you Like to ship </h2>
                           
                        </div>

<!--yes no switch-->



<div class="container">
    <div class="row">
       
<form method="POST" action="{{url('/pricing_plans4')}}">


    {{  csrf_field() }}
   
    <div class="form-group">
        <div class="col-sm-10 ">

            <h1 class="underline-small control-label  ">What Best Describe This Shipment ???</h1>
        </div> 

             <div class="col-sm-12">

                <textarea class="form-control " rows="2" name="description" ></textarea>

             </div>
  
     </div>

     <div class="form-group">
        <div class="col-sm-10 ">

            <h1 class="underline-small control-label  ">What Are You Shipping ???</h1>
        </div> 

             <div class="col-sm-12">

                <textarea class="form-control " rows="3" name="item_that_is_shipping" ></textarea>

             </div>
  
     </div>
     <div class="form-group">
        <div class="col-sm-10 ">

            <h1 class="underline-small control-label  "> Add Notification And Other Option </h1>
        </div> 

        <div class="col-md-12" >
           
                
                    <div>
                         <h2 class="title-1"> Required Signeture   </h2>
                        <label class=" switch1">
                                      <input type="checkbox" id="togBtn" name="signeture_required"  value="1" checked>
                                       <div class="slider1 round">      
                                       </div>


                         </label>

                        
                    </div>
        


     </div>


       <div class="col-md-12" >
           
                
                    <div>
                         <h2 class="title-1"> Sunday Delidery   </h2>
                        <label class=" switch1">
                                      <input type="checkbox" name="sunday_delivary" value="1" id="togBtn" checked>
                                       <div class="slider1 round">      
                                       </div>


                               </label>

                        
                    </div>
        


     </div>

     

     

                        
  </div>

                              
                    
    </div>
   <div class="form-group">
    <div class="form-group wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s" >
        <div class="col-sm-12  col-sm-offset-2 custombtn " >
          <button name="submit" id="submit_btn" class="btn btn-success title-1"> Continue</button>
    </div>
    </div>
               
        


     </div>

 

  
     



</form>





 <div class="form-group">
    <div class="form-group wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s" >
        <div class="col-sm-2 col-sm-offset-2 custombtn " >
          <button name="submit" id="submit_btn" class="btn btn-info title-1"> Back </button>
    </div>
    </div>
       
  
       
   </div>
   <div class="form-group">
    <div class="form-group wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s" >
        <div class="col-sm-2 custombtn" >
          <button class="btn btn-danger title-1" name="submit" id="submit_btn" > cancel shipment</button>
    </div>
    </div>
    

</div>
</div>
</div>

  



  




@endsection