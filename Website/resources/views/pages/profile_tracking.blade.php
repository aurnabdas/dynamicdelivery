@extends('profile_layout')
@section('pplayout')

<div class="page-wrapper">
                <div class="content container-fluid">
					<div class="row">
						<div class="col-sm-8">
							<h4 class="page-title">Tracking</h4>
						</div>
						
					</div>
					<div class="row filter-row">
						<form action="profile_tracking" method="post" role="search">
							{{ csrf_field() }}

							<div class="col-sm-3 col-md-2 col-xs-6">  
								<div class="form-group form-focus">
									<label class="control-label">Tracking Id</label>
									<input type="text" class="form-control floating" name="trackid"/>
									
								</div>
						   </div>
							<div class="col-sm-3 col-xs-6">  
								<button type="submit" class="btn btn-success btn-block" >
									<!-- <a href="trackingresults" > --> Search  <!-- </a>  --> 
								</button>
								
							</div>

						</form>

						     
                    </div>

                    <div class="row">
						<div class="col-md-12">
							<div class="table-responsive">
								<table class="table table-striped custom-table m-b-0">
									<thead>
										<tr>
											
											<th>Package ID</th>
                                            <th>Sender</th>
                                            <th>Sending Date</th>
                                            <th>Delivery Date</th>
                                            <th>Total</th>
                                            <th>Delivery Status</th>
                                            <th>Carrier Allocated</th>
                                            <th>status</th>
										</tr>
									</thead>
									<tbody>
										@foreach( $tracking_packages as $packag)
                                                    
                                                    
                                                            <tr>
                                                                <td><a href="invoice-view">{{ $packag->packageid }}</a></td>
                                                                <td>
                                                                    <h2><a href="#">{{ $packag->sender_name }}</a></h2>
                                                                </td>
                                                                <td>{{ $packag->sending_date }}</td>
                                                                <td>{{ $packag->arrival_date }}</td>
                                                                <td>{{ $packag->total_price }}</td>
                                                                <td>
                                                                	 @if($packag->completed == 1  )
                                                                            <span class="label label-success-border">Success</span>

                                                                        @else
                                                                            <span class="label label-warning-border">Pending</span>
                                                                        @endif
                                                                    
                                                                </td>
                                                                <td>
                                                                    @if($packag->allocated == 1)
                                                                   		 <span class="label label-success-border">Allocated</span>

                                                                    @else
                                                                        <span class="label label-warning-border">Not Allocated</span>
                                                                    @endif
                                                                     
                                                                    
                                                                </td>
                                                                <td>
                                                                	@if($packag->active == 0)
                                                                                Inactive

                                                                            @else
                                                                                Active
                                                                            @endif
                                                                </td>
                                                            </tr>
                                                        
                                                    
                                                @endforeach

										<!-- <tr>
											<td>1</td>
											<td><a href="invoice-view">#INV-0001</a></td>
											<td>Global Technologies</td>
											<td>1 Sep 2017</td>
											<td>7 Sep 2017</td>
											<td>$2099</td>
											<td><span class="label label-success-border">Paid</span></td>
											<td class="text-right">
												<div class="dropdown">
													<a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
													<ul class="dropdown-menu pull-right">
														<li><a href="edit-invoice"><i class="fa fa-pencil m-r-5"></i> Edit</a></li>
														<li><a href="invoice-view"><i class="fa fa-eye m-r-5"></i> View</a></li>
														<li><a href="#"><i class="fa fa-file-pdf-o m-r-5"></i> Download</a></li>
														<li><a href="#"><i class="fa fa-trash-o m-r-5"></i> Delete</a></li>
													</ul>
												</div>
											</td>
										</tr> -->									
										
									</tbody>
								</table>
							</div>
						</div>
					</div>					
                </div>
				
            </div>


@endsection 