@extends('profile_layout')
@section('pplayout')


<div class="page-wrapper">
                <div class="content container-fluid">
					<div class="row">
						<div class="col-sm-8">
							<h4 class="page-title">Tracking</h4>
						</div>
						
					</div>
					<div class="row filter-row">
						<div class="col-sm-3 col-md-2 col-xs-6">  
							<div class="form-group form-focus">
								<label class="control-label">Tracking Id</label>
								<input type="text" class="form-control floating" />
							</div>
					   </div>
						<div class="col-sm-3 col-xs-6">  
							<a href="#" class="btn btn-success btn-block"> Search </a>  
						</div>     
                    </div>
					<div class="row">
						<div class="col-md-12">
							<div class="table-responsive">
								<table class="table table-striped custom-table m-b-0">
									<thead>
										<tr>
											<th>#</th>
											<th>Invoice Number</th>
											<th>Client</th>
											<th>Created Date</th>
											<th>Due Date</th>
											<th>Amount</th>
											<th>Status</th>
											<th class="text-right">Action</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td><a href="invoice-view.html">#INV-0001</a></td>
											<td>Global Technologies</td>
											<td>1 Sep 2017</td>
											<td>7 Sep 2017</td>
											<td>$2099</td>
											<td><span class="label label-success-border">Paid</span></td>
											<td class="text-right">



@endsection 