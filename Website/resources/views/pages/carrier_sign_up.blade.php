  
@extends('f')
@section('frontpages')
 <body id="home">

        <!-- Main Wrapper -->        
        <main class="wrapper">

            <!-- Header -->
            <header class="header-main">

                <!-- Header Topbar -->
                <div class="top-bar font2-title1 white-clr">
                    <div class="theme-container container">
                        <div class="row">
                            <div class="col-md-6 col-sm-5">
                                <ul class="list-items fs-10">
                                    <li><a href="#"></a></li>
                                    <li class="active"><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                </ul>
                            </div>
                            <div class="col-md-6 col-sm-7 fs-12">
                                <p class="contact-num">  <i class="fa fa-phone"></i> Call us now: <span class="theme-clr">+880-162-565-5354 </span> </p>
                            </div>
                        </div>
                    </div>
                    <a data-toggle="modal" href="#login-popup" class="sign-in fs-12 theme-clr-bg"> sign in </a>
                </div>
                <!-- /.Header Topbar -->

                <!-- Header Logo & Navigation -->
                <nav class="menu-bar font2-title1">
                    <div class="theme-container container">
                        <div class="row">
                            <div class="col-md-2 col-sm-2">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-controls="navbar">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-logo" href="#"> <img src="assets/img/logo/logo-black.png" alt="logo" /> </a>
                            </div>
                            <div class="col-md-10 col-sm-10 fs-12">
                                <div id="navbar" class="collapse navbar-collapse no-pad">
                                    <ul class="navbar-nav theme-menu">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" >Home </a>
                                     
                                        </li>
                                        <li> <a href="about-us.html">about</a> </li>
                                       <!-- <li> <a href="tracking.html"> tracking </a> </li>
                                        <li> <a href="pricing-plans.html"> pricing </a> </li> -->
                                        <li class="active"> <a href="contact-us.html"> contact </a> </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" >sign up</a>
                                           
                                        </li>
                                        
                                         
                                        <li><span class="search fa fa-search theme-clr transition"> </span></li>
                                    </ul>                                                      
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
                <!-- /.Header Logo & Navigation -->

            </header>


             <div class="title-wrap text-center  pb-50">
                            <h2 class="section-title wow fadeInUp" data-wow-offset="50" data-wow-delay=".20s">Carrier sign Up From</h2>
                        </div>


                         

                         <div class="row " >
                             <div class="col-sm-12 col-sm-offset-2" >
                                 <p >* Indicate Required Filed</p>
                             </div>
                         </div>




<div class="container">
    <div class="row">
       
<form method="POST" action="{{url('/carrier_sign_up')}}">

  {{  csrf_field()  }}

     <div class="form-horizontal" >
        <div class="form-group wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s">
        <label class="control-label title-2  col-sm-2 "  > Name: *</label>
       <div class="col-sm-4">
            <input class="form-control" type="text" id="inputusername" name="name">
       </div>
        <label class="control-label title-2  col-sm-2 "  >Passport Number: *</label>
       <div class="col-sm-4">
            <input class="form-control" type="text" id="inputusername" name="passport_num">
       </div>
   </div>       
    </div>

      <div class="form-horizontal" >
        <div class="form-group wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s">
        <label class="control-label title-2  col-sm-2 "  >Id Number  : *</label>
       <div class="col-sm-4">
            <input class="form-control" type="text" id="inputusername" name="nid">
       </div>
        <label class="control-label title-2  col-sm-2 "  >Date Of Birth: *</label>
       <div class="col-sm-4">
            <input class="form-control" type="date" id="inputusername" name="date_of_birth">
       </div>
   </div>
    </div>
     <div class="form-horizontal" >
        <div class="form-group wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s">
        <label class="control-label title-2  col-sm-2 "  >Email : *</label>
       <div class="col-sm-4">
            <input class="form-control" type="text" id="inputusername" name="email">
       </div>
        <label class="control-label title-2  col-sm-2 "  >Flight Number : *</label>
       <div class="col-sm-4">
            <input class="form-control" type="text" id="inputusername" name="flight_num">
       </div>
   </div>
    </div>
     <div class="form-horizontal" >
        <div class="form-group wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s">
        <label class="control-label title-2  col-sm-2 "  >Phone Number   : *</label>
       <div class="col-sm-4">
            <input class="form-control" type="text" id="inputusername" name="contact_num">
       </div>
        <label class="control-label title-2  col-sm-2 "  >Departure Time : *</label>
       <div class="col-sm-4">
            <input class="form-control" type="Time" id="inputusername" name="departure_time">
       </div>
   </div>
    </div>
     <div class="form-horizontal" >
        <div class="form-group wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s">
        <label class="control-label title-2  col-sm-2 "  >Arrival date  : *</label>
       <div class="col-sm-4">
            <input class="form-control" type="date" id="inputusername" name="arrival_date">
       </div>
        <label class="control-label title-2  col-sm-2 "  >Arrival Time : *</label>
       <div class="col-sm-4">
            <input class="form-control" type="Time" id="inputusername" name="arrival_time">
       </div>
   </div>
    </div>
     <div class="form-horizontal" >
        <div class="form-group wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s">
        <label class="control-label title-2  col-sm-2 "  >Departure date   : *</label>
       <div class="col-sm-4">
            <input class="form-control" type="date" id="inputusername" name="departure_date">
       </div>
        <label class="control-label title-2  col-sm-2 "  >Weight Can Carry:*</label>
       <div class="col-sm-4">
            <input class="form-control" type="text" id="inputusername" name="carried_waight">
       </div>
   </div>
    </div>

     <div class="form-horizontal" >
        <div class="form-group wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s">
        <label class="control-label title-2  col-sm-2 "  >Traveling From    : *</label>
       <div class="col-sm-4">
            <input class="form-control" type="text" id="inputusername" name="travel_from">
       </div>
        <label class="control-label title-2  col-sm-2 "  >Traveling To: *</label>
       <div class="col-sm-4">
            <input class="form-control" type="text" id="inputusername" name="travel_to">
       </div>
   </div>
    </div>
  


       <div class="form-horizontal" >
       <div class="form-group wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s"  >
            <label class="control-label title-2  col-sm-2 "  >Address: *</label>
       <div class="col-sm-10">
            <input class="form-control" type="text" id="inputusername" name="address">
       </div>
       </div>
    </div>
      <div class="form-horizontal" >
       <div class="form-group wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s"  >
            <label class="control-label title-2  col-sm-2 "  ></label>
       <div class="col-sm-10">
            <input class="form-control" type="text" id="inputusername" name="address">
       </div>
       </div>
    </div>
     <div class="form-horizontal" >
       <div class="form-group wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s"   >
            <label class="control-label title-2  col-sm-2 "  ></label>
       <div class="col-sm-10">
            <input class="form-control" type="text" id="inputusername" name="address">
       </div>
       </div>
    </div>

     <div class="form-horizontal" >
        <div class="form-group wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s">
    
        <label class="control-label title-2  col-sm-3"  >Product Pick Up Location : *</label>
       <div class="col-sm-6">
            <input class="form-control" type="text" id="inputusername" name="product_pick_up_location">
       </div>
   </div>
    </div>


<div class="col-sm-10 col-sm-offset-2" >
            <ul class="">
                <li class="title-3"><input type="checkbox" value="1" name="term_condition"> Terms And Condition </li>
            </ul>
        </div>

   <div class="form-group">
    <div class="form-group wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s" >
        <div class="col-sm-2 col-sm-offset-2 custombtn " >
          <button name="submit" id="submit_btn" class="btn btn-success title-1"> Submit </button>
    </div>
    </div>
       
   </div>
  
  


</form>

</div>
</div>

            <section class="pad-30 more-about-wrap">
                                <div class="theme-container container pb-100">               
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 wow fadeInUp" data-wow-offset="50" data-wow-delay=".20s">
                                            <div class="more-about clrbg-before">
                                                <h2 class="title-1">How To Work</h2>
                                                <div class="pad-10"></div>
                                                <p></p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s">
                                            <div class="more-about clrbg-before">
                                                <h2 class="title-1">About Insurance Policy</h2>
                                                <div class="pad-10"></div>
                                                <p></p>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </section>
          

@endsection