  
@extends('welcome')
@section('content')


<body id="home">
	<main class="wrapper">

            <!-- Header -->
            <header class="header-main">

                <!-- Header Topbar -->
                <div class="top-bar font2-title1 white-clr">
                    <div class="theme-container container">
                        <div class="row">
                            <div class="col-md-6 col-sm-5">
                                <ul class="list-items fs-10">
                                    <li><a href="#"></a></li>
                                    <li class="active"><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                </ul>
                            </div>
                            <div class="col-md-6 col-sm-7 fs-12">
                                <p class="contact-num">  <i class="fa fa-phone"></i> Call us now: <span class="theme-clr"> +880-1756-390-370 </span> </p>
                            </div>
                        </div>
                    </div>
                    <a data-toggle="modal" href="#login-popup" class="sign-in fs-12 theme-clr-bg"> sign in </a>
                </div>
                <!-- /.Header Topbar -->

                <!-- Header Logo & Navigation -->
                <nav class="menu-bar font2-title1">
                    <div class="theme-container container">
                        <div class="row">
                            <div class="col-md-2 col-sm-2">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-controls="navbar">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-logo" href=""> <img src="{{('assets/img/logo/logo-black.png')}}" alt="logo" /> </a>
                            </div>
                            <div class="col-md-10 col-sm-10 fs-12">
                                <div id="navbar" class="collapse navbar-collapse no-pad">
                                    <ul class="navbar-nav theme-menu">
                                        <li class="dropdown">
                                            <a href="{{URL::to('/')}}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" >Home </a>
                                           
                                        </li>
                                        <li> <a href="{{URL::to('/about_us')}}">about</a> </li>
                                        <li class="active"> <a href="{{URL::to('/usertracking')}}"> tracking </a> </li>
                                        <li> <a href="{{URL::to('/pricing_plans')}}"> pricing </a> </li>
                                        <li> <a href="{{URL::to('/contact_us')}}"> contact </a> </li>
                                        <li class="dropdown">
                                            <a href="{{URL::to('/sign_up')}}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" >sign up</a>
                                           
                                        </li>
                                         
                                        <li><span class="search fa fa-search theme-clr transition"> </span></li>
                                    </ul>                                                      
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
                <!-- /.Header Logo & Navigation -->

            </header>
            <!-- /.Header -->

            
<div class="work">
    <div class="step">
    <ul class="progressbar" >
        <li class="active title-1 " >Where </li>
        <li class="title-1" >what</li>
        <li class="title-1" >how</li>
        <li class="title-1">Details</li>
        <li class="title-1">Payment</li>
        <li class="title-1">Review</li>
        
    </ul>
    
</div>
</div>












                      <div class="title-wrap text-center  pb-50">
                            <h2 class="section-title wow fadeInUp underline-small2" data-wow-offset="50" data-wow-delay=".20s">How Would you Like to pay</h2>
                           
                        </div>

<!--yes no switch 

<a class="btn btn1 stycol" data-toggle="collapse" href="#collapseExample" role="button"aria-expanded="false" aria-controls="collapseExample">   
                            Bill From My Account
             </a>



  <div class="col-md-12" >
           
                
                    <div>
                         <h2 class="title-1"> Required Signeture   </h2>
                        <label class=" switch1">
                                      <input type="checkbox" id="togBtn" checked>
                                       <div class="slider1 round">      
                                       </div>


                               </label>

                        
                    </div>
        


     </div>



        <div class="form-horizontal" >
                        <div class="form-group wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s">
                        <label class="control-label title-2  col-sm-2 "  >weight:</label>
                        <div class="col-md-6" >
                
                    <div class="col-md-6">

                        <label class=" switch1">
                                      <input type="checkbox" id="togBtn" checked>
                                       <div class="slider1 round">
                                            
                                       </div>


                               </label>

                        
                    </div>

     </div>
                        <label class="control-label title-2  col-sm-2 "  >length:</label>
                       <div class="col-sm-3">
                            <input class="form-control" type="" id="inputusername">
                       </div>
                   </div>
                    </div>



                     <div class="form-horizontal" >
                       <div class="form-group wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s"  >
                            <label class="control-label title-2  col-sm-2 "  >decleared value:</label>
                       <div class="col-sm-6">
                            <input class="form-control" type="" id="inputusername">
                       </div>
                       </div>
                    </div>



-->


                              

 

<div class="container">
     <div class="row">
                       
        <p class="pedit">
              <button class="btn btn-primary stycol title-2" data-toggle="collapse" href="#multiCollapseExample1" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">Bill From My Account</button>
           
             <button class="btn btn-primary stycol title-2" type="button" data-toggle="collapse" data-target="#multiCollapseExample2" aria-expanded="false" aria-controls="multiCollapseExample2">
                          Pay on The Cash  
              </button>
                          <button class="btn btn-primary stycol title-2" type="button" data-toggle="collapse" data-target="#multiCollapseExample3" aria-expanded="false" aria-controls="multiCollapseExample3">
                           Pay By The Card
                          </button>
        </p>
                <div class="collapse multi-collapse" id="multiCollapseExample1">
                  <div class="card">
                  <div class="card-body">

                    <div class="title-wrap text-center  pb-50">
                            <h2 class="section-title wow fadeInUp" data-wow-offset="50" data-wow-delay=".20s">Do Payment from my Account</h2>
                           
                        </div>

                    <div class="container">
                    <div class="row">
                       
                <form method="POST" action="{{url('/pricing_plans5')}}">
                  
                     <div class="col-md-8 col-md-offset-2 tracking-form wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s">     
                                <h2 class="title-1"> Choose An Account </h2> 
                                <div class="row">
                                    
                                        <div class="col-md-7 col-sm-7">
                                            <div class="form-group">
                                                <input type="text" placeholder="Account"  class="form-control box-shadow" name="bill">
                                            </div>
                                        </div>
                                      
                                    
                                </div>
                            </div> 
                                      
                                    

                   
                    


                   <div class="col-md-8 col-md-offset-2" >
                     <div class="col-md-4">
                         <h2 class="title-1"> Use A Promo Code    </h2>
                         <label class=" switch1">
                                      <input type="checkbox" name="promocode" value="1" id="togBtn" >
                                       <div class="slider1 round">      
                                       </div>


                               </label>

                        </div>
                        <div class="col-md-4">
                         <h2 class="title-1"> Enter The Promo Code   </h2>
                        
                     
            <input class="form-control" type="text" id="inputusername" name="promocode_is">
       



                        
                        </div>

        
                   </div>
                 
 

                </form>



                </div>
                </div>
                  </div>
                </div>
            </div>

               <div class="collapse multi-collapse" id="multiCollapseExample2">
                  <div class="card">
                  <div class="card-body">

                    <div class="title-wrap text-center  pb-50">
                            <h2 class="section-title wow fadeInUp" data-wow-offset="50" data-wow-delay=".20s">do payment by Cash </h2>
                           
                        </div>

                    <div class="container">
                    <div class="row">
                       
                <form>
                
                   <div class="col-md-8 col-md-offset-2" >
                     <div class="col-md-4">
                        
                         <label class=" switch1">
                                      <input type="checkbox" name="cash" value="1" id="togBtn" checked>
                                       <div class="slider1 round">      
                                       </div>


                               </label>

                        </div>
                      

        
                   </div>
                 
 

                </form>



                </div>
                </div>
                  </div>
                </div>
            </div>




            <div class="collapse multi-collapse" id="multiCollapseExample3">
                  <div class="card">
                  <div class="card-body">

                    <div class="title-wrap text-center  pb-50">
                            <h2 class="section-title wow fadeInUp" data-wow-offset="50" data-wow-delay=".20s">Do Payment by Card</h2>
                           
                        </div>

                    <div class="container">
                    <div class="row">
                       
                <form method="POST" action="{{url('/pricing_plans5')}}">

                  {{  csrf_field()  }}

           

           <div class="col-md-8 col-md-offset-2 tracking-form wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s">     
                                <h2 class="title-1">Choose the card type </h2> 
                                <div class="row">
                                    
                                        <div class="col-md-7 col-sm-7">
                                            <div class="form-group">
                                                <select name="card_type" class="form-control" id="exampleFormControlSelect1">
                                                          <option value="Master Card">Master Card </option>
                                                          <option value="Visa">Visa</option>
                                                          <option value="Pay Pal">Pay Pal</option>
                                                          <option value="American Exress">American Exress</option>
                                                          
                                                 </select>
                                            </div>
                                        </div>
                                      
                                    
                                </div>
                            </div> 



                <div class="col-md-8 col-md-offset-2" >
                     <div >
                         <h2 class="title-1"> Enter The Card Number     </h2>
                         <input class="form-control" type="text" id="inputusername" name="card_num">

                        </div>
            
        
                   </div>


                   <div class="col-md-8 col-md-offset-2" >
                     <div class="col-md-4">
                         <h2 class="title-1"> MM/YY   </h2>
                         <input class="form-control" type="text" id="inputusername" name="card_exp">

                        </div>
                        <div class="col-md-4">
                         <h2 class="title-1"> CVV/Cvc   </h2>
                        
                     
            <input class="form-control" type="text" id="inputusername" name="card_cc">
       



                        
                        </div>

        
                   </div>


                 
 

                



                </div>
                </div>
                  </div>
                </div>
            </div>




             <div class="form-group">
                <div class="form-group wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s" >
                    <div class="col-sm-2 col-sm-offset-2 custombtn " >
                      <button name="submit" id="submit_btn" class="btn btn-info title-1"> Back </button>
                </div>
                </div>
                   
               </div>
               <div class="form-group">
                <div class="form-group wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s" >
                    <div class="col-sm-2 custombtn " >
                      <button name="submit" id="submit_btn" class="btn btn-success title-1"> Continue</button>
                </div>
                </div>
                   
               </div>
               <div class="form-group">
                <div class="form-group wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s" >
                    <div class="col-sm-2 custombtn" >
                      <button class="btn btn-danger title-1" name="submit" id="submit_btn" > cancel shipment</button>
                </div>
                </div>
                

            </div>

          </form>


    </div>
</div>





@endsection