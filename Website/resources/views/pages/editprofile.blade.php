@extends('profile_layout')
@section('pplayout')
<form method="POST"  enctype="multipart/form-data" action="{{url('/editprofile')}}">


	{{csrf_field()}}

	
       <div class="page-wrapper">
                <div class="content container-fluid">
					<div class="row">
						<div class="col-sm-8">
							<h4 class="page-title">Edit Profile</h4>
						</div>
					</div>
					<form>
						<div class="card-box">
							<h3 class="card-title">Basic Informations</h3>
							<div class="row">
								<div class="col-md-12">
									<div class="profile-img-wrap">
										<img class="inline-block" src="" alt="user">
										<div class="fileupload btn btn-default">
											<span class="btn-text">edit</span>
											
												
												<input class="upload" name="image" type="file">
											
											
										</div>
									</div>  
									<div class="profile-basic">
										<div class="row">
											<div class="col-md-12" style="text-align: center;color: black;">
												<div class="form-group form-focus" >
										            <!--  <label class="control-label" ">Enter Details</label> -->
									               	<input style="text-align: center;" type="text" class="form-control floating" value="Enter Details"  readonly>
									            </div>
											</div>
											<!-- <div class="col-md-6">
												<div class="form-group form-focus">
													<label class="control-label">Last Name</label>
													<input type="text" class="form-control floating" name="l_name" />
												</div>
											</div> -->
											<div class="col-md-6">
												<div class="form-group form-focus">
										             <label class="control-label">First Name </label>
									               	<input type="text" class="form-control floating"  name="f_name">
									            </div>
											</div>
											<div class="col-md-6">
												<div class="form-group form-focus">
													<label class="control-label">Last Name</label>
													<input type="text" class="form-control floating" name="l_name" />
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group form-focus">
													<label class="control-label">Birth Date</label>
													<div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="date_of_birth"></div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group form-focus select-focus">
													<label class="control-label">Gendar</label>
													<select name="gender" class="select form-control floating">
														<option value="">Select Gendar</option>
														<option value="">Male</option>
														<option value="">Female</option>
													</select>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="card-box">
							<h3 class="card-title">Contact Informations</h3>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group form-focus">
										<label class="control-label">Address</label>
										<input type="text" class="form-control floating" name="address" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group form-focus">
										<label class="control-label">State</label>
										<input type="text" class="form-control floating" name="State" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group form-focus">
										<label class="control-label">Country</label>
										<input type="text" class="form-control floating" name="Country" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group form-focus">
										<label class="control-label">Pin Code</label>
										<input type="text" class="form-control floating" name="post_code" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group form-focus">
										<label class="control-label">Phone Number</label>
										<input type="text" class="form-control floating"  name="contact_number" />
									</div>
								</div>
							</div>
						</div>

						<div class="text-center m-t-20">
							<button class="btn btn-primary btn-lg" type="submit">Save &amp; update</button>
						</div>
	}
					
	
</form>


@endsection 