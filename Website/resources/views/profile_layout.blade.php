<!DOCTYPE html>
<html>
    
<!-- Mirrored from dreamguys.co.in/smarthr/light/invoice-view.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 15 May 2018 06:55:22 GMT -->
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <link rel="shortcut icon" type="image/x-icon" href="assets-2/img/favicon.png">
        <title>Invoice - HRMS admin template</title>
		<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="assets-2/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="assets-2/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="assets-2/css/style.css">
		<!--[if lt IE 9]>
			<script src="assets-2/js/html5shiv.min.js"></script>
			<script src="assets-2/js/respond.min.js"></script>
		<![endif]-->
    </head>
    <body>
        <div class="main-wrapper">
            <div class="header">
                <div class="header-left">
                    <a href="index.html" class="logo">
                        <img src="assets-2/img/logo.png" width="40" height="40" alt="">
                    </a>
                </div>
                <div class="page-title-box pull-left">
                    <h3></h3>
                </div>
                <a id="mobile_btn" class="mobile_btn pull-left" href="#sidebar"><i class="fa fa-bars" aria-hidden="true"></i></a>
                <ul class="nav navbar-nav navbar-right user-menu pull-right">
                    <!-- <li class="dropdown hidden-xs">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell-o"></i> <span class="badge bg-purple pull-right">3</span></a>
                        <div class="dropdown-menu notifications">
                            <div class="topnav-dropdown-header">
                                <span>Notifications</span>
                            </div>
                            <div class="drop-scroll">
                                <ul class="media-list">
                                    <li class="media notification-message">
                                        <a href="activities.html">
                                            <div class="media-left">
                                                <span class="avatar">
                                                    <img alt="John Doe" src="assets-2/img/user.jpg" class="img-responsive img-circle">
                                                </span>
                                            </div>
                                            <div class="media-body">
                                                <p class="m-0 noti-details"><span class="noti-title">John Doe</span> added new task <span class="noti-title">Patient appointment booking</span></p>
                                                <p class="m-0"><span class="notification-time">4 mins ago</span></p>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="media notification-message">
                                        <a href="activities.html">
                                            <div class="media-left">
                                                <span class="avatar">V</span>
                                            </div>
                                            <div class="media-body">
                                                <p class="m-0 noti-details"><span class="noti-title">Tarah Shropshire</span> changed the task name <span class="noti-title">Appointment booking with payment gateway</span></p>
                                                <p class="m-0"><span class="notification-time">6 mins ago</span></p>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="media notification-message">
                                        <a href="activities.html">
                                            <div class="media-left">
                                                <span class="avatar">L</span>
                                            </div>
                                            <div class="media-body">
                                                <p class="m-0 noti-details"><span class="noti-title">Misty Tison</span> added <span class="noti-title">Domenic Houston</span> and <span class="noti-title">Claire Mapes</span> to project <span class="noti-title">Doctor available module</span></p>
                                                <p class="m-0"><span class="notification-time">8 mins ago</span></p>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="media notification-message">
                                        <a href="activities.html">
                                            <div class="media-left">
                                                <span class="avatar">G</span>
                                            </div>
                                            <div class="media-body">
                                                <p class="m-0 noti-details"><span class="noti-title">Rolland Webber</span> completed task <span class="noti-title">Patient and Doctor video conferencing</span></p>
                                                <p class="m-0"><span class="notification-time">12 mins ago</span></p>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="media notification-message">
                                        <a href="activities.html">
                                            <div class="media-left">
                                                <span class="avatar">V</span>
                                            </div>
                                            <div class="media-body">
                                                <p class="m-0 noti-details"><span class="noti-title">Bernardo Galaviz</span> added new task <span class="noti-title">Private chat module</span></p>
                                                <p class="m-0"><span class="notification-time">2 days ago</span></p>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="topnav-dropdown-footer">
                                <a href="activities.html">View all Notifications</a>
                            </div>
                        </div>
                    </li>
                    <li class="dropdown hidden-xs">
                        <a href="javascript:;" id="open_msg_box" class="hasnotifications"><i class="fa fa-comment-o"></i> <span class="badge bg-purple pull-right">8</span></a>
                    </li>  -->  
                    <li class="dropdown">
                        <a href="profile.html" class="dropdown-toggle user-link" data-toggle="dropdown" title="Admin">
                            <span class="user-img"><img class="img-circle" src="assets-2/img/user.jpg" width="40" alt="Admin">
                            <span class="status online"></span></span>
                            
                            <i class="caret"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="">My Profile</a></li>
                            <li><a href="">Edit Profile</a></li>
                            <li><a href="">Settings</a></li>
                            <li><a href="">Logout</a></li>
                        </ul>
                    </li>
                </ul>
                <div class="dropdown mobile-user-menu pull-right">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="profile.html">My Profile</a></li>
                        <li><a href="edit-profile.html">Edit Profile</a></li>
                  
                        <li><a href="login.html">Logout</a></li>
                    </ul>
                </div>
            </div>
            <div class="sidebar" id="sidebar">
                <div class="sidebar-inner slimscroll">
                    <div id="sidebar-menu" class="sidebar-menu">
                            
                        <ul >
                            <li style="margin-top: 20px;">
                                <div class="container">
                                    <div class="row" style="padding-left: 20px;">
                                        <div class="col-sm-2 ">
                                                <img src="assets-2/img/user-03.jpg" width="150" height="150" alt="" style="border-radius: 50%">
                                        </div>
                                    </div>
                                    <div class="row" style="padding-left: 20px;">
                                        <div class="col-sm-2">
                                            <h3>Aurnab Das</h3> 
                                        </div>
                                    </div>
                                   
                                    
                                    
                                </div>
                            </li>
                            
                            <li class="active">
                            <a href="{{URL::to('/profile')}}" class=""><span> About</span> </a>
                              
                            </li>
                            <li> 
                                <a href="{{URL::to('/profile_claim')}}">Claim</a>
                            </li>
                             <li> 
                                <a href="{{URL::to('/profile_send')}}">send </a>
                            </li>
                           
                           
                            <li> 
                                <a href="{{URL::to('/profile_tracking')}}">Tracking</a>
                            </li>
                          
                            <li> 
                                <a href="{{URL::to('/profile_report')}}">Report</a>
                            </li>
                            <!-- <li> 
                                <a href="leads.html">Product Claims</a>
                            </li> -->
                           
                           
                        </ul>
                    </div>
                </div>
            </div>

             @yield('pplayout')


        </div>
		<div class="sidebar-overlay" data-reff="#sidebar"></div>
        <script type="text/javascript" src="assets-2/js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="assets-2/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="assets-2/js/jquery.slimscroll.js"></script>
		<script type="text/javascript" src="assets-2/js/app.js"></script>
    </body>

<!-- Mirrored from dreamguys.co.in/smarthr/light/invoice-view.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 15 May 2018 06:55:22 GMT -->
</html>