<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//frontend route

use Illuminate\Support\Facades\Input;

Route::get('/',function () {
    return view('pages.global_home');
});
Route::get('/index', 'HomeController@index');
Route::get('/about_us','NavigationController@show_about');
//Route::get('/usertracking','NavigationController@show_usertracking');
Route::get('/pricing_plans','NavigationController@show_pricing_plans');
Route::get('/contact_us','NavigationController@show_contact_us');

Route::get('/sign_up','NavigationController@show_sign_up');//->middleware('auth')
Route::get('/pricing_plans1','NavigationController@show_pricing_plans1');
Route::get('/pricing_plans2','NavigationController@show_pricing_plans2');
Route::get('/pricing_plans3','NavigationController@show_pricing_plans3');
Route::get('/pricing_plans4','NavigationController@show_pricing_plans4');
Route::get('/pricing_plans5','NavigationController@show_pricing_plans5');
Route::get('/pricing_plans6','NavigationController@show_pricing_plans6');
Route::get('/carrier_sign_up','NavigationController@show_carrier_sign_up');
Route::get('/profile','NavigationController@show_profile');
Route::get('/profile_claim','NavigationController@show_profile_claim');

Route::get('/profile_send','NavigationController@show_profile_send');
Route::get('/editprofile','NavigationController@show_editprofile');
Route::get('/profile_recieve','NavigationController@show_profile_recieve');

Route::get('/profile_tracking_result','NavigationController@show_profile_tracking_result');
Route::get('/profile_tracking','NavigationController@show_profile_tracking');


Route::get('/profile_report','NavigationController@show_profile_report');
Route::get('/test','HomeController@show_test');
Route::get('/login','NavigationController@show_login');

Route::get('/usertracking','HomeController@gettracking');


Route::get('/global_home', function () {
    return view('pages.global_home');
});

Route::get('/logout','HomeController@show_logout');

//reset password route

Route::get('password/reset/{token?}');
Route::get('password/email');
//datainsert route
Route::post('/sign_up','DatainsertController@signupdata');
Route::post('/login','HomeController@login_user');

Route::post( '/carrier_sign_up','DatainsertController@carrier_sign_up_data');


Route::post('/pricing_plans','DatainsertController@pricing_plans1_data_in');
Route::post('/pricing_plans1','DatainsertController@pricing_plans2_data_in');
Route::post('/pricing_plans2','DatainsertController@pricing_plans3_data_in');
Route::post('/pricing_plans3','DatainsertController@pricing_plans4_data_in');
Route::post('/pricing_plans4','DatainsertController@pricing_plans5_data_in');
Route::post('/pricing_plans5','DatainsertController@pricing_plans6_data_in');
Route::post('/editprofile','DatainsertController@edit_profile_data_in');
Route::post('/profile_claim','DatainsertController@profile_claim_data_in');
Route::post('/profile_report','DatainsertController@profile_report_data_in');

Route::post('/usertracking','HomeController@posttracking');
Route::post('/profile_tracking','NavigationController@post_profile_tracking');
Route::post('/contact_us','DatainsertController@show_contact_us');



Route::post('/global_home', function () {
    $user= Input::get('user');
    $country= Input::get('country');

    if ($user == 'send') {

    	return view('pages.sign_up');
    	# code...
    }
    else
    {
    	return view('pages.carrier_sign_up');
    	
    }


    /*return view('login');*/
});

