<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->integer('packageid')->unsigned()->nullable();
            $table->integer('weight');
            $table->string('type');
            $table->integer('length');
            $table->integer('width');
            $table->integer('decleared_value');

            $table->integer('taxable_price')->unsigned()->nullable();
            $table->boolean('taxable_item')->default(0);

            $table->string('description')->nullable();
            $table->string('item_that_is_shipping')->nullable();
            $table->string('signeture_required')->default(0);
            $table->string('sunday_delivary')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
