<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claims', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Sender_Name');
            $table->string('Parcel_Number');
            $table->string('Tracking_Id');
            $table->integer('Number_of_Packages');
            $table->string('total_waight');
            $table->string('Decleared_Value');
            $table->string('Description_About_the_damage');
            $table->string('insurance_number');
            $table->string('Mobile_Number');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('claims');
    }
}
