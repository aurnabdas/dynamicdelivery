<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->integer('id')->nullable()->unsigned();
             $table->string('f_name')->nullable();
             $table->string('l_name')->nullable();
              $table->string('date_of_birth')->nullable();
               $table->string('gender')->nullable();
                $table->string('address')->nullable();
                 $table->string('State')->nullable();
                  $table->string('Country')->nullable();
                   $table->string('post_code')->nullable();
                    $table->string('contact_number')->nullable();
                     $table->string('avatar')->default('default.jpg');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
