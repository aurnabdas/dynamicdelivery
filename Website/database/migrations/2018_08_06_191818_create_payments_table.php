<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->integer('id')->unsigned()->nullable();

            $table->integer('bill')->unsigned()->nullable();
            $table->string('cash')->default(0);
            
            $table->string('promocode')->default(0);
             $table->string('promocode_is')->nullable();
            $table->string('card_type')->nullable();
            $table->string('card_num')->nullable();
            $table->string('card_exp')->nullable();
            $table->string('card_cc')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
