<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
           
            $table->integer('packageid');

            $table->string('total_items')->nullable();
            $table->string('total_price')->nullable();
            $table->string('tax_price')->nullable();
            $table->string('taxable_product')->nullable();
            $table->string('sender_id')->nullable();
            $table->string('sender_name')->nullable();
            $table->string('sending_dest')->nullable();
            $table->string('recieving_dest')->nullable();
            $table->string('total_num_carriers')->nullable();
            $table->string('carrier_id')->nullable();
            $table->string('carrier_name')->nullable();
            $table->string('carrier_amount')->nullable();
            $table->string('carrier_pickup_dest')->nullable();
            $table->string('carrier_drop_dest')->nullable();
            $table->string('package_route')->nullable();


            $table->string('home_pickup')->default(0);
            $table->string('office_drop')->default(0);
            $table->string('expressservice')->default(0);

            $table->string('branch_id_from')->nullable();
            $table->string('branch_id_to')->nullable();
            $table->string('sending_date')->nullable();
            $table->string('arrival_date')->nullable();
            $table->boolean('allocated')->default(0);
            $table->string('tracking_num')->nullable();
            $table->timestamps();
            $table->boolean('completed')->default(0);
            $table->boolean('report')->default(0);
            $table->string('report_details')->nullable();
            $table->string('report_type')->nullable();
            $table->boolean('report_solved')->default(0);
            $table->boolean('active')->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
