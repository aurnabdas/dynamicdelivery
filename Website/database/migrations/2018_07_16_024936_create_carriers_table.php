<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarriersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carriers', function (Blueprint $table) {
             $table->integer('id')->nullable();
             $table->string('name');
             $table->string('nid');
             $table->string('email')->unique();
             $table->string('contact_num');
            $table->string('departure_date');
            $table->string('arrival_date');
            $table->string('travel_from');
            $table->string('travel_to');
            $table->string('address');
            $table->string('passport_num');
            $table->string('date_of_birth');
            $table->string('flight_num');
            $table->string('departure_time');         
            $table->string('arrival_time');
            $table->string('carried_waight'); 
            $table->string('product_pick_up_location');
            $table->string('term_condition')->default(0);
            $table->string('branch_id_from');
            $table->string('branch_id_to');   
        
            
           /**
            $table->string('departure_time');
            $table->string('departure_dest');
            $table->string('arrival_date');
            $table->string('arrival_time');
            $table->string('arrival_dest');
           
            $table->string('arrival_airport');
          
            $table->integer('packageid');
            $table->integer('carrierid');

           */
                        
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carriers');
    }
}
