<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\sender;
use App\user;
use App\package;
use App\item;
use App\carrier;
use App\payment;
use App\profile;
use App\claim;
use App\report;
use App\contact;
use App\package_sending_detail;
use App\package_recieving_detail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;

Use Session;
use DB;
use App\branch;
session_start();

class DatainsertController extends Controller
{
   


     public function signupdata(Request $request)
    {
                $user = new user();
                $user->name= $request->input("first_name");
			    $user->username= $request->input("last_name");
			    $user->email = $request->input("email");
			    //$user->password= Hash::make($request->input("password"));
			    $user->password= $request->input("password");
			    $user->country = $request->input("country");
			    $user->date_of_birth= $request->input("date_of_birth");
			    $user->gender = $request->input("gender");
			    $user->save();



			   $sender = new sender();
			   $sender->id = $user->id;
			   $sender->first_name = $request->input("first_name");
			   $sender->last_name = $request->input("last_name");
			   $sender->passport_num =$request->input("passport_num");
			   $sender->nid =$request->input("nid");
			   $sender->address=$request->input("address");
			   $sender->email =$request->input("email");
			   $sender->contact_num =$request->input("contact_num");
			   $sender->account_num =$request->input("account_num");
			   $sender->password =$request->input("password"); 
			   $sender->save();

               Session::put('message','Please Log in');
			   return Redirect::to('login');

			
   }


   public function pricing_plans1_data_in(Request $request)
   {


			   $package_sending_detail = new package_sending_detail();
			   $ui=Session::get('id');
			   
			   $package_sending_detail->name = $request->input("name");
			   $package_sending_detail->country = $request->input("country");
			   $package_sending_detail->city =$request->input("city");
			   $package_sending_detail->state =$request->input("state");
			   $package_sending_detail->postal_code=$request->input("postal_code");
			   $package_sending_detail->email =$request->input("email");
			   $package_sending_detail->contact_num =$request->input("contact_num");
			   $package_sending_detail->address =$request->input("address");
			  
               $checkvalue =input::has('return_address') ? true : false;
			   $package_sending_detail->return_address=$checkvalue;

			   $package_sending_detail->nearest_airport =$request->input("nearest_airport"); 


               
			   $package_sending_detail->save();

               Session::put('packageid', $package_sending_detail->id);
               Session::put('id', $ui);
			   return Redirect::to('pricing_plans1');



   }

 
              public function pricing_plans2_data_in(Request $request)
               {

               $package_recieving_detail = new package_recieving_detail();
                 

               $ui=Session::get('id');
               $y=Session::get('packageid');
                   
               $package_recieving_detail->packageid =$y;
			  
			   $package_recieving_detail->name = $request->input("name");
			   $package_recieving_detail->country = $request->input("country");
			   $package_recieving_detail->city =$request->input("city");
			   $package_recieving_detail->state =$request->input("state");
			   $package_recieving_detail->postal_code=$request->input("postal_code");
			   $package_recieving_detail->email =$request->input("email");
			   $package_recieving_detail->contact_num =$request->input("contact_num");
			   $package_recieving_detail->address =$request->input("address");



			   $package_recieving_detail->save();

               Session::put('packageid', $y);
               Session::put('id', $ui);
			   return Redirect::to('pricing_plans2');


   }



        public function pricing_plans3_data_in(Request $request)
               {

	               $item = new item();
	               $y=Session::get('packageid');
	                 
	               $num_item=Input::get('total');
	               $q=(int) $num_item;
	               $ui=Session::get('id');

	               if ($num_item>=1) {
	               		$num_item=Input::get('total');
	               		//return Redirect::to('test');
                 
	               		

	               		$item->packageid =$y;
							  
							   $item->type= $request->input("type");
							   $item->weight = $request->input("weight");
							   $item->length =$request->input("length");
							   $item->width =$request->input("width");
							   $item->decleared_value =$request->input("decleared_value");



							   $item->save();

	               		for($i=1;$i<$q;$i++){

				               $item = new item();

				               $item->packageid =$y;
							   	
							 /*  	$xx= "type".$i;
							   	$xy= "weight".$i;
							   	$xz= "length".$i;
							   	$xc= "decleared_value".$i;*/



							   $item->type= $request->input("type".$i);

							   $item->weight = $request->input("weight".$i);
							   $item->length =$request->input("length".$i);
							   $item->decleared_value =$request->input(	"length".$i);
							    $item->width =$request->input("width".$i);



							   $item->save();
							   /*Session::put('num', $xx);
							   Session::put('num1', $xy);
							   Session::put('num2', $xz);
							   return Redirect::to('test');*/
			               }

			               /*Session::put('num', 'aaa');
							return Redirect::to('test');*/

	               }
	               else{
	               		$num_item = "-0" ;
	               		//return Redirect::to('pricing_plans2');

	               		$item->packageid =$y;
							  
							   $item->type= $request->input("type");
							   $item->weight = $request->input("weight");
							   $item->length =$request->input("length");
							   $item->decleared_value =$request->input("decleared_value");
                                $item->width =$request->input("width");


							   $item->save();
	               }

	               

	               

	               /*$y=Session::get('packageid');
	                   
	               $item->packageid =$y;
				  
				   $item->type= $request->input("type");
				   $item->weight = $request->input("weight");
				   $item->length =$request->input("length");
				   $item->decleared_value =$request->input("decleared_value");*/



				  /* $item->save();*/

	               Session::put('num', $y);
	               Session::put('id', $ui);
				   return Redirect::to('pricing_plans3');


  				}

				public function carrier_sign_up_data(Request $request)
				{
				     
				   $carrier = new carrier();

							    $ui=Session::get('id');
                                $y=Session::get('packageid');
                   
                                $carrier->id =$ui;
							   
							   $carrier->name = $request->input("name");
							   $carrier->passport_num = $request->input("passport_num");
							   $carrier->nid =$request->input("nid");
							   $carrier->date_of_birth =$request->input("date_of_birth");
							   $carrier->email=$request->input("email");
							   $carrier->flight_num =$request->input("flight_num");
							   $carrier->contact_num =$request->input("contact_num");
							   $carrier->departure_time =$request->input("departure_time");
							   $carrier->arrival_date =$request->input("arrival_date"); 

							   $carrier->arrival_time =$request->input("arrival_time");
							   $carrier->departure_date =$request->input("departure_date");
							   $carrier->carried_waight =$request->input("carried_waight");
							   $carrier->travel_from =$request->input("travel_from"); 


							   $carrier->travel_to =$request->input("travel_to");
							   $carrier->address =$request->input("address");
    						   $carrier->product_pick_up_location =$request->input("product_pick_up_location");

							    $tandm =input::has('term_condition') ? true : false;
							    $carrier->term_condition=$tandm;


                               $result=DB::table('branches')->get();

                               	foreach ($result as $results) {
                               		# code...
                               		if ($results->city == $carrier->travel_from  ) {
                               			# code...
                               			$carrier->branch_id_from=$results->branchid;

                               		}
                               		if ($results->city == $carrier->travel_to  ) {
                               			# code...
                               			$carrier->branch_id_to=$results->branchid;

                               		}
                                       
                               	}
                            




                               	    Session::put('id', $ui);
                               	    Session::put('packageid', $y);

							   $carrier->save();

							   
							   return Redirect::to('/index');

				}

				public function pricing_plans4_data_in()
				{
					$package = new package();
                    $ui=Session::get('id');
					$y=Session::get('packageid');
					 $package->packageid =$y;					
					 $pickup =input::has('pickup') ? true : false;
			         $package->home_pickup=$pickup;
			          $deliver =input::has('deliver') ? true : false;
			           $package->office_drop=$deliver;

                       $expressservice =input::has('expressservice') ? true : false;
			           $package->expressservice=$expressservice;

					   
					   $package->save();
					   Session::put('packageid', $y);
					    Session::put('id', $ui);
					   return Redirect::to('/pricing_plans4');
				}
              
                        

               
                    

					public function  pricing_plans5_data_in(Request $request){

				    $item=new item();
					$y = Session::get('packageid');
					 $ui=Session::get('id');
					$item->packageid = $y;

					$result = DB::table('items')->where('packageid','=',$y)
					        ->update([ 
					            'description' => Input::get('description'),
					            'item_that_is_shipping' => Input::get('item_that_is_shipping'),
					            'sunday_delivary' => (bool) input::has('sunday_delivary'),
					            'signeture_required' => (bool) input::has('signeture_required'),
					        ]);


					         Session::put('packageid', $y);
					          Session::put('id', $ui);
					   return Redirect::to('/pricing_plans5');
					}
										



					
			public function pricing_plans6_data_in(Request $request)


			{
                  $payment = new payment();

					$y=Session::get('packageid');
					 $ui=Session::get('id');
				// $payment->packageid =$y;	
	

			    $check =input::has('promocode') ? true : false;
			   $payment->promocode=$check;
			        
                 $check1 =input::has('cash') ? true : false;
			   $payment->cash=$check1;

			   $payment->bill =$request->input("bill");
			   $payment->promocode_is =$request->input("promocode_is");
			   $payment->card_type =$request->input("card_type");


			    $payment->card_num =$request->input("card_num");
			   $payment->card_exp =$request->input("card_exp");
			   $payment->card_cc =$request->input("card_cc");
					   
		        $payment->save();
					   Session::put('packageid', $y);
					    Session::put('id', $ui);

   

    











					   return Redirect::to('/pricing_plans6');

			}		



			public function edit_profile_data_in(Request $request)
			{ 

                

               $profile = new profile();

					$y=Session::get('packageid');
					 $ui=Session::get('id');
					
	

			   

			   $profile->id =$ui;
			   $profile->f_name =$request->input("f_name");
			   $profile->l_name =$request->input("l_name");
			 //  $profile = $request->file('image')->store('upload');


			   $profile->date_of_birth =$request->input("date_of_birth");
			   $profile->gender =$request->input("gender");
			   $profile->address =$request->input("address");


			    $profile->State =$request->input("State");
			   $profile->Country =$request->input("Country");
			   $profile->post_code =$request->input("post_code");
			   $profile->contact_number =$request->input("contact_number");
			   
					   
					   
		        $profile->save();

		        
					   Session::put('packageid', $y);
					    Session::put('id', $ui);
         return Redirect::to('/profile');


			}			






			public function profile_claim_data_in(Request $request)
			{

                   $claim = new claim();

					$y=Session::get('packageid');
					$ui=Session::get('id');
					
	

			   

			  
			   $claim->Sender_Name =$request->input("Sender_Name");
			   $claim->Parcel_Number =$request->input("Parcel_Number");


			   $claim->Tracking_Id =$request->input("Tracking_Id");
			   $claim->Number_of_Packages =$request->input("Number_of_Packages");
			   $claim->total_waight =$request->input("total_waight");


			    $claim->Decleared_Value =$request->input("Decleared_Value");
			   $claim->Description_About_the_damage =$request->input("Description_About_the_damage");
			   $claim->insurance_number =$request->input("insurance_number");
			   $claim->Mobile_Number =$request->input("Mobile_Number");
			   
					   
					   
		        $claim->save();
					   Session::put('packageid', $y);
					    Session::put('id', $ui);
         return Redirect::to('/profile');


			}	



public function profile_report_data_in(Request $request)

{
      
                    


                     $report = new report();

					$y=Session::get('packageid');
					$ui=Session::get('id');
					
	

			   

			  
			   $report->tracking_id =$request->input("tracking_id");
			   $report->email =$request->input("email");


			   $report->subject =$request->input("subject");
			   $report->message =$request->input("message");
			   $report->type =$request->input("type");


             $result = DB::table('packages')->where('tracking_num','=',$report->tracking_id)
					        ->update([ 
					            'report' => 1,
					            'report_details' => $report->message,
					            'report_type' => $report->type ,
					            'report_solved' => 0,
					        ]);


			
					   
					   
		        $report->save();
					   Session::put('packageid', $y);
					    Session::put('id', $ui);
         return Redirect::to('/profile');


}


public function show_contact_us(Request  $request)
{
     $contact=new contact();


    $contact->Name=$request->input("Name");
      $contact->Email=$request->input("Email");
        $contact->Phone=$request->input("Phone");
          $contact->Message=$request->input("Message");

          $contact->save();


return Redirect::to('/index');

}






}


				