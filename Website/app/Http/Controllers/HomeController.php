<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\package;
use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\Input;


Use Session ;
session_start();

class HomeController extends Controller
{
   public function index(){


 
/*
      $ui=Session::get('id');
      $name=Session::get('name');*/

      return view('pages.home_content');

   }

   public function login_user(Request $request)
   { 
         
   	    $email=Input::get('email');
          /*$password=md5(Input::get('Password'));*/
   	    $password=Input::get('Password');
   	    $result=DB::table('users')->where(['email'=>$email,'password'=>$password ])->first();

   	    if ($result) {
   	    	# code...
           
           Session::put('message','password and email is correct');
           Session::put('id',$result->id);
           Session::put('email',$result->email);
           Session::put('password',$result->password);
            Session::put('name',$result->name);
   	    	 return Redirect::to('/index');
   	    }
         
   	    else
   	    {  

   	    	Session::put('message','password and email is  not correct');
   	    	return Redirect::to('/sign_up');
   	    }

   }

   public function show_test()
   {
      $carrier=DB::table('carriers')->get();

   // dd($carrier);
      
      foreach ($carrier as $carriers) {
        //$data['branch_id_to']='$carriers->branch_id_to';
     $temp=['$carriers->branch_id_to'];
    
     
}



   	   return view('pages.test',compact(temp) );
   }

    public function gettracking(){

      $trackid='null';
      $tracking_packages = package::tracking_packages($trackid);
      
      $z=0;
      foreach ($tracking_packages as $packag) {
        $z=$packag->packageid;
        # code...
      }
      $i=0;
      $item=DB::table('items')->where('packageid','=',$z)->get();

      return view('pages.usertracking',compact('tracking_packages','item','i'));

   }

   public function posttracking(){

      $trackid= Input::get('trackid');
      $tracking_packages = package::tracking_packages($trackid);
      
      $z=0;
      foreach ($tracking_packages as $packag) {
        $z=$packag->packageid;
        # code...
      }
      $i=0;
      $item=DB::table('items')->where('packageid','=',$z)->get();

      /*foreach ($tracking_packages as $packag) {
        $item=DB::table('items')->where('packageid','=',$packag->packageid)->get();
        # code...
      }*/

      //$item=DB::table('items')->where('packageid','=',$y)->get();
 
      return view('pages.usertracking',compact('tracking_packages','item','i'));

   }

   public function show_logout()
   {
     
           Session::flush();

      return Redirect::to('/login');

   }



}
 